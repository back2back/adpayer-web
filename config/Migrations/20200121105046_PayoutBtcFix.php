<?php
use Migrations\AbstractMigration;

class PayoutBtcFix extends AbstractMigration
{

    public function up()
    {
    	$table = $this->table('payouts');

    	$table->addColumn('btc_amount', 'decimal', [
    		'default' => null,
            'null' => false,
            'precision' => 16,
             'scale' => 8,
         ]);
        
        $table->addColumn('coin', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 8,
            
        ]);
        $table->update();
    }

    public function down()
    {
    }
}

