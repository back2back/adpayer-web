<?php
use Migrations\AbstractMigration;
//use Phinx\Db\Adapter\MysqlAdapter;

class Withfaq extends AbstractMigration
{

    public function up()
    {

        $this->table('payouts')
            ->changeColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->update();

        $this->table('articles')
            ->addColumn('topic', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('body', 'text', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('user_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('is_faq', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('referrals')
            ->addColumn('user_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('lft', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('rght', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('reset_tokens')
            ->addColumn('token', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('is_valid', 'integer', [
                'default' => '1',
                'limit' => 4,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('expires', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('windows')
            ->addColumn('window_status', 'integer', [
                'default' => '1',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('window_key', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('start_time', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('end_time', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('user_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('amt', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 8,
                'scale' => 2,
            ])
            ->create();

        $this->table('payouts')
            ->addColumn('window_key', 'string', [
                'after' => 'user_id',
                'default' => null,
                'length' => 255,
                'null' => false,
            ])
            ->addColumn('profile_id', 'string', [
                'after' => 'window_key',
                'default' => null,
                'length' => 255,
                'null' => false,
            ])
            ->addColumn('status', 'integer', [
                'after' => 'profile_id',
                'default' => '0',
                'length' => 4,
                'null' => false,
            ])
            ->addColumn('payeer_ref', 'string', [
                'after' => 'modified',
                'default' => null,
                'length' => 16,
                'null' => false,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('payouts')
            ->changeColumn('id', 'string', [
                'default' => null,
                'length' => 16,
                'null' => false,
            ])
            ->removeColumn('window_key')
            ->removeColumn('profile_id')
            ->removeColumn('status')
            ->removeColumn('payeer_ref')
            ->update();

        $this->table('articles')->drop()->save();

        $this->table('referrals')->drop()->save();

        $this->table('reset_tokens')->drop()->save();

        $this->table('windows')->drop()->save();
    }
}

