<?php
use Migrations\AbstractMigration;

class Payoutid extends AbstractMigration
{

    public function up()
    {

        $this->table('referral_earnings')
            ->addColumn('parent', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('child', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('amount', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 8,
                'scale' => 2,
            ])
            ->addColumn('referral_status', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('payout_id', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->create();
    }

    public function down()
    {

        $this->table('referral_earnings')->drop()->save();
    }
}

