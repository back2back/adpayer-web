<?php
use Migrations\AbstractMigration;

class ProfileBlobs extends AbstractMigration
{

    public function up()
    {

        $this->table('profiles')
            ->changeColumn('btc_address', 'binary', [
                'default' => null,
                'length' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();

        $this->table('purchases')
            ->changeColumn('id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->update();

        $this->table('coinpayments')
            ->addColumn('amount', 'decimal', [
                'default' => null,
                'null' => false,
                'precision' => 16,
                'scale' => 8,
            ])
            ->addColumn('transaction', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('confirms_needed', 'integer', [
                'default' => null,
                'limit' => 3,
                'null' => false,
            ])
            ->addColumn('timeout', 'integer', [
                'default' => null,
                'limit' => 6,
                'null' => false,
            ])
            ->addColumn('checkout_url', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('status_url', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('qrcode_url', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('user_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addIndex(
                [
                    'transaction',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('purchases')
            ->addColumn('provider', 'string', [
                'after' => 'modified',
                'default' => null,
                'length' => 32,
                'null' => true,
            ])
            ->addColumn('reference', 'integer', [
                'after' => 'provider',
                'default' => null,
                'length' => 11,
                'null' => false,
            ])
            ->addColumn('status', 'integer', [
                'after' => 'reference',
                'default' => '0',
                'length' => 2,
                'null' => false,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('profiles')
            ->changeColumn('btc_address', 'string', [
                'default' => null,
                'length' => 255,
                'null' => false,
            ])
            ->update();

        $this->table('purchases')
            ->changeColumn('id', 'string', [
                'default' => null,
                'length' => 16,
                'null' => false,
            ])
            ->removeColumn('provider')
            ->removeColumn('reference')
            ->removeColumn('status')
            ->update();

        $this->table('coinpayments')->drop()->save();
    }
}

