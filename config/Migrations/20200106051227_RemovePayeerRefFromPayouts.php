<?php
use Migrations\AbstractMigration;

class RemovePayeerRefFromPayouts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('payouts');
        $table->removeColumn('payeer_ref');
        $table->addColumn('btc_address', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255,
            
        ]);
        $table->update();
    }
}
