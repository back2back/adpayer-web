<?php
namespace App\Utility;

use Cake\Log\Log;
use Cake\ORM\TableRegistry;

class CommissionCalculator
{
	public function getParentDetails($childId, $amount){
		Log::write('error', $childId);
		//find parent;
		//what node
		$referrals = TableRegistry::getTableLocator()->get('Referrals');

		$nodeId = null;

		$query = $referrals->find('all')
				->where(['Referrals.user_id' => $childId])
				->limit(1);

		$refData = null;
		foreach($query as $goodData){
			$refData = $goodData->parent_id;
		}
		/*foreach($query as $row){
			$nodeId = $row->id;
		}*/

		

		//with the node find path to parent, only 1 level deep for now
		//bug
		//$toParentQuery = $referrals->find('path', ['for' => $nodeId])
		//				->limit(1);

		$toParentQuery = $referrals->find('all')
						->where(['id' => $refData ]);

		
		foreach($toParentQuery as $data){
			$parentId = $data->id; //id in the referral table. don't confuse with below
			$parentUserId = $data->user_id; //user_id for the parent

			Log::write('error', 'foo ' .$parentUserId);

			//get the parents subscription
			$subscriptions = TableRegistry::getTableLocator()->get('Subscriptions');
			$subsQuery = $subscriptions->find('all')
						->where(['Subscriptions.user_id' => $parentUserId])
						->limit(1);
			
			foreach($subsQuery as $subData){
				Log::write('error', 'We have subscription data '. $subData->end_date);
			}
			
			//what package?
			$packages = TableRegistry::getTableLocator()->get('Packages');
			$packagesQuery = $packages->find('all')
							->where(['Packages.id' => $subData->package_id])
							->limit(1);
			
			foreach($packagesQuery as $packageData){
				Log::write('error', 'Parent package is '. $packageData->name);
			}

			$commission = $packageData->commission;
			Log::write('error', 'Commission in percentage ' .$commission);

			$referralEarning = ($commission/100) * $amount;
			
			$referralEarningsTable = TableRegistry::getTableLocator()->get('ReferralEarnings');

			$referralEarningEntity = $referralEarningsTable->newEntity();
			$referralEarningEntity->amount = $referralEarning;
			$referralEarningEntity->parent = $parentUserId;
			$referralEarningEntity->child = $childId;

			if ($referralEarning > 0) {
				
				$referralEarningsTable->save($referralEarningEntity);
			}

			
			

		}
	}
}