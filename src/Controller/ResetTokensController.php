<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\NotFoundException;

/**
 * ResetTokens Controller
 *
 * @property \App\Model\Table\ResetTokensTable $ResetTokens
 *
 * @method \App\Model\Entity\ResetToken[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ResetTokensController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add','validate']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $resetTokens = $this->paginate($this->ResetTokens);

        $this->set(compact('resetTokens'));
    }

    /**
     * View method
     *
     * @param string|null $id Reset Token id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $resetToken = $this->ResetTokens->get($id, [
            'contain' => []
        ]);

        $this->set('resetToken', $resetToken);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $resetToken = $this->ResetTokens->newEntity();
        if ($this->request->is('post')) {
            $resetToken = $this->ResetTokens->patchEntity($resetToken, $this->request->getData());
            if ($this->ResetTokens->save($resetToken)) {
                $this->Flash->success(__('Check your registered email inbox'));

                //return $this->redirect(['action' => 'mail_success']);
            }
            //$this->Flash->error(__('The reset token could not be saved. Please, try again.'));
        }
        $this->set(compact('resetToken'));
    }

   

    public function validate($token){
        $data = $this->ResetTokens->find('token', ['token' => $token]);
        
        if (is_null($data)) {
            //return $this->redirect(['controller'=>'users' ,'action' => 'login']);
            throw new NotFoundException(__('Error Processing Request'));
            
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $myData = $this->request->getData();
            $updatedPassword = $myData['password'];
            //debug($updatedPassword);
           $success = $this->ResetTokens->updateToken($data, $updatedPassword);
            if ($success) {
                return $this->redirect(['controller' => 'users', 'action' => 'login']);
            }else{
                $this->Flash->error(__('The password could not be updated.'));
            } 
        }  
    }
    public function mailSuccess(){
        //show users to check the email inbox
    }
}
