<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;


class ApiController extends AppController
{
    public function initialize(){
        parent::initialize();
        //$this->Security->setConfig('unlockedActions', ['*']);
    }

    public function isAuthorized($user)
    {
        $prefix = $this->request->getParam('prefix');

        if ($prefix === 'api') {
            
            return true;
        }
        return false;
    }
    
    
}
