<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;

class PlaylistsController extends ApiController{


    public function initialize()
    {
        parent::initialize();
        
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Security->setConfig('unlockedActions', ['addToWatched']);
    }

    public function index(){
        $playlists = $this->Playlists->find('all');
        $this->set([
            'playlists' => $playlists,
            '_serialize' => ['playlists']
        ]);
    }

	public function addToWatched($dailymotionVideo)
    {
        
            $this->request->allowMethod(['post', 'put']);

            $playlist = $this->Playlists->newEntity();
            $playlist->user_id = $this->Auth->user('id');
            $playlist->video_ref = $this->request->getData('video_ref');
           
            if ($this->Playlists->save($playlist)) {
                $message = 'Saved';
            } else {
                $message = 'Error';
            }
            $user = $this->Auth->user('id');
            $this->set([
                'message' => $message,
                'playlist' => $playlist,
                'user' => $user,
                '_serialize' => ['message', 'playlist','user']
            ]);    

    }

    public function isAuthorized($user){
        $action = $this->request->getParam('action');
        if (in_array($action, ['addToWatched','index'])) {
            return true;
        }
    }
}