<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Windows Controller
 *
 * @property \App\Model\Table\WindowsTable $Windows
 *
 * @method \App\Model\Entity\Window[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WindowsController extends AdminController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
            'order' => [
                'Windows.id' => 'desc',
            ]
        ];
        $windows = $this->paginate($this->Windows);

        $this->set(compact('windows'));
    }

    /**
     * View method
     *
     * @param string|null $id Window id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $window = $this->Windows->get($id, [
            //'contain' => ['Payouts']
        ]);

        
        $payoutsTable = TableRegistry::getTableLocator()->get('Payouts');
        $query = $payoutsTable->find()->where(['window_key' => $window->window_key]);
        $profilesTable = TableRegistry::getTableLocator()->get('Profiles');

        $emptybla = array();
        //this query looks up the payouts table
        foreach($query as $someData){
            //debug($someData);
            //someData is payoutsData

            $profileQuery = $profilesTable->find()->where(['id' => $someData->profile_id]);
            $profileData = ($profileQuery->toArray());
            //debug($profileData);
            array_push($emptybla, $profileData);
            
            
        }
        
        
        $this->set('window', $window);
        $this->set('payouts', $query);
    }

    public function close($id){
        
        $window = $this->Windows->get($id);

        $status = $window->window_status;
        //status true is open false otherwise
        //cannot close an already closed window
        if (!$status) {
            $this->Flash->error(__('Cannot close an already closed window'));
            return $this->redirect(['action' => 'index']);

        }

        $payoutsTable = TableRegistry::getTableLocator()->get('Payouts');
        $query = $payoutsTable->find()->where(['window_key' => $window->window_key]);

        $amount = 0;
        foreach($query as $payoutData){
            $amount += $payoutData->amount;

            //set the payout data to processed
            $payoutData->status = 1;
            $payoutsTable->save($payoutData);
        }
        $window->window_status = 0;
        $window->amt = $amount;

        if ($this->Windows->save($window)) {
            $this->Flash->success(__('Window closed. Please download CSV and process payments '));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function export($id){
        $window = $this->Windows->get($id, [
            //'contain' => ['Payouts']
        ]);

        $payoutsTable = TableRegistry::getTableLocator()->get('Payouts');
        $query = $payoutsTable->find()->where(['Payouts.window_key' => $window->window_key]);

        

        $data = $query->toArray();
        $_extract = [
            'coin',
            'btc_address',
            'btc_amount'
        ];
        

        
        $_serialize = ['data'];
        $this->viewBuilder()->setClassName('CsvView.Csv');
        $this->set(compact('data', '_serialize', '_extract'));  
       
    }
    

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $window = $this->Windows->newEntity();
        if ($this->request->is('post')) {
            $window->user_id = $this->Auth->user('id');
            $window = $this->Windows->patchEntity($window, $this->request->getData());
            if ($this->Windows->save($window)) {
                $this->Flash->success(__('The window has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The window could not be saved. Please, try again.'));
        }
        $users = $this->Windows->Users->find('list', ['limit' => 200]);
        $this->set(compact('window', 'users'));
    }

    
    
}
