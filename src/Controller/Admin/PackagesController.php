<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class PackagesController extends AdminController{

	public function index(){
        $packages = $this->paginate($this->Packages);
        $this->set('packages', $packages);
    }

    public function edit($id = null){
    	$package = $this->Packages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $package = $this->Packages->patchEntity($package, $this->request->getData());
            if ($this->Packages->save($package)) {
                $this->Flash->success(__('The package has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The package could not be saved. Please, try again.'));
        }
        $this->set(compact('package'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $package = $this->Packages->newEntity();
        if ($this->request->is('post')) {
            $package->user_id = $this->Auth->user('id');
            $package = $this->Packages->patchEntity($package, $this->request->getData());
        
            if ($this->Packages->save($package)) {
                $this->Flash->success(__('The package has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The package could not be saved. Please, try again.'));
        }
        $this->set(compact('package'));
    }

    public function foobar(){
        echo 'test';
    }
}