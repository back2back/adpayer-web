<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Log\LogTrait;

class PurchasesController extends AdminController{
	use LogTrait;

	public function initialize(){
        parent::initialize();
    }

	public function index(){
		$purchases = $this->Purchases->find('all');
		$this->set('purchases', $purchases);
	}

	public function view($id){

		$purchase = $this->Purchases->get($id);
		$this->set('purchase', $purchase);
	}

	public function edit($id){
		$purchase = $this->Purchases->findById($id)->firstOrFail();
			


		if ($this->request->is(['post', 'put'])) {

			$this->Purchases->patchEntity($purchase, $this->request->getData());
			$this->log((string) $purchase);
			if ($this->Purchases->save($purchase)) {
				$this->Flash->success(__('Saved successfully'));
				return $this->redirect(['action' => 'index']);
			}
			$this->Flash->error(__('Did not update'));
		}

		$this->set('purchase', $purchase);

	}

	public function delete(){

	}
}