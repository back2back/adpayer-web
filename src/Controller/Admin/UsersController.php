<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;
use Cake\I18n\Time;
use Cake\I18n\Date;

class UsersController extends AdminController{

	
	public function logout()
    {
        
        return $this->redirect($this->Auth->logout());
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect(['action' => 'dashboard', 'admin' => true]);
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
    }

    public function index()
    {
        $this->paginate = [
            'contain' => ['Profiles','Payouts']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }


    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Profiles', 'Playlists', 'Purchases']
        ]);

        $this->set('user', $user);
    }

    public function add(){
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('User created successfully'));

                return $this->redirect(['action' => 'dashboard']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        //$profiles = $this->Users->Profiles->find('list', ['limit' => 200]);
        //$packages = $this->Users->Packages->find('list', ['limit' => 200]);
        $this->set(compact('user', 'profiles', 'packages'));
    }

    public function dashboard(){
        //get count of users
        $userCount = $this->Users->find()->count();

        //packages
        $packagesTable = TableRegistry::getTableLocator()->get('Packages');
        $packagesQuery = $packagesTable->find('all');
        $packageCount = $packagesTable->find()->count();


        //subscription
        $subscriptionCount = $this->Users->Subscriptions->find()->count();

        //subQuery
        $subQuery = $this->Users->Subscriptions->find();

        $collection = new Collection($subQuery->all());
        $subByGroup = $collection->countBy('package_id');


        /*$subByGroup = $subByGroup->map(function($value, $key){
            return $value;
        });*/
        $subByGroup = $subByGroup->toArray();
       

        $queryActive = $subQuery->where(['package_id >' => 1])->count();

        $windowsTable = TableRegistry::getTableLocator()->get('Windows');
        $windowsQuery = $windowsTable->find();

        
            $dateToday = new Time();
            $lastMonth = $dateToday->subMonths(1);
            
            $begin = new Time($lastMonth->startOfMonth());
            $end = new Time($lastMonth->endOfMonth());

            
            $lastMonthQuery = TableRegistry::getTableLocator()->get('Windows')

                ->find('all')
                ->where(['created >' => $begin, 'created <' => $end]);

            $lastMonthAmount = 0.00;
            foreach($lastMonthQuery as $data){
                $lastMonthAmount += $data->amt;
                
            }

        $purchaseTable = TableRegistry::getTableLocator()->get('Purchases');
        $purchasesQuery = $purchaseTable->find();

        $lastMonthIncomeQuery = TableRegistry::getTableLocator()->get('Purchases')
            ->find('all')
            ->where(['created >' => $begin, 'created <' => $end]);

            
        $lastMonthIncome = 0.00;
        foreach($lastMonthIncomeQuery as $data){
            $lastMonthIncome += $data->amount;
            
        }

            

        $this->set('userCount', $userCount);
        $this->set('packageCount', $packageCount);
        $this->set('subscriptionCount', $subscriptionCount);
        $this->set('activeSubs', $queryActive);
        $this->set('subByGroup', $subByGroup);
        $this->set('packagesQuery', $packagesQuery);
        $this->set('windowsQuery', $windowsQuery);
        $this->set('lastMonthAmount', $lastMonthAmount);
        $this->set('lastMonthIncome', $lastMonthIncome);
        $this->set('purchasesQuery', $purchasesQuery);


    }
}