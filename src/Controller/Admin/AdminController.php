<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;


class AdminController extends AppController
{
    public function initialize(){
        parent::initialize();
    }

    public function isAuthorized($user)
    {
        $prefix = $this->request->getParam('prefix');
        if ($prefix === 'admin' && $user['is_admin'] == true) {
            //admin can access all controller actions
            return true;
        }
        return false;
    }
    
    
}
