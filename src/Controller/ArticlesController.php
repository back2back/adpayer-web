<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

class ArticlesController extends AppController
{
	
	public function initialize(){
		parent::initialize();
		$this->Auth->allow(['index','view']);

		
	}

	public function beforeRender(Event $event){
		$this->viewBuilder()->setLayout('no_layout');
	}

	/**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        //$this->Articles->recover();
        $articles = $this->paginate($this->Articles);

        $articles = $this->Articles->find('all');

        $this->set(compact('articles'));
    }

    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $article = $this->Articles->get($id, [
            //'contain' => ['Users']
        ]);
        
        $this->set('article', $article);
    }

    public function isAuthorized($user){
        $action = $this->request->getParam('action');
        
        if (in_array($action, [''])) {
            return true;
        }
    }
}