<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Utility\Security;
use Cake\Log\Log;

class SettingsController extends AppController
{
	
	public function initialize(){
		parent::initialize();
	}

	

	/**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function overview()
    {
        
        $session = $this->getRequest()->getSession();
        $user = $session->read('Auth.User');

        //get profile 
        $profiles = TableRegistry::getTableLocator()->get('Profiles');
        $profileData = $profiles->find('all')->where(['user_id' => $user['id']])->firstOrFail();

        //debug($profileData->btc_address);
        

        if ($this->request->is(['patch', 'post', 'put'])){
            $profileData = $profiles->patchEntity($profileData, $this->request->getData());
            //debug($this->request->getData());
            if ($profiles->save($profileData)) {
                $this->Flash->success(__('Changes saved.'));

                

                return $this->redirect(['controller'=>'payouts', 'action' => 'add']);
            }
        }
        $this->set('profileData', $profileData);
    }

    

    public function isAuthorized($user){
        $action = $this->request->getParam('action');
        
        if (in_array($action, ['overview'])) {
            return true;
        }
    }
}