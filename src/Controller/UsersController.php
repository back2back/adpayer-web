<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout','add']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    /*public function index()
    {
        $this->paginate = [
            'contain' => ['Profiles']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }
*/
    

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $referral = $this->request->getQuery('referral');
        //debug($referral);
        $user = $this->Users->newEntity();
        $user->referralId = $referral;
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Login using your details'));

                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        //$profiles = $this->Users->Profiles->find('list', ['limit' => 200]);
        //$packages = $this->Users->Packages->find('list', ['limit' => 200]);
        $this->set(compact('user', 'profiles', 'packages'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /*public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $profiles = $this->Users->Profiles->find('list', ['limit' => 200]);
        $packages = $this->Users->Packages->find('list', ['limit' => 200]);
        $this->set(compact('user', 'profiles', 'packages'));
    }*/

    
    public function login()
    {

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);

                $user = $this->Auth->user();
                if ($user['is_admin'] == 1) {
                    # code...
                    return $this->redirect(['action' => 'dashboard', 'prefix' => 'admin']);
                }else{
                   return $this->redirect($this->Auth->redirectUrl()); 
                }
                
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
    }
    public function logout()
    {
        
        return $this->redirect($this->Auth->logout());
    }

    public function dashboard(){
        $user = $this->Auth->user();

        $userQuery = $this->Users->find('all')
                    ->where(['Users.id' => $user['id']])
                    ->contain(['Profiles']);
        $userEntity = $userQuery->first();
        $myLastVideosQuery = $this->Users->Playlists->find('myVideosSinceLastPayout', ['user' => $userEntity]);

        $count = 0;
        $earningsSinceLastPayout = 0;
        foreach($myLastVideosQuery as $data){
            $count++;
            $earningsSinceLastPayout += $data->earn;
        }

        //my profile
        $profileData = $userEntity->profile;

        //last payout
        $payoutsTable = TableRegistry::getTableLocator()->get('Payouts');
        $payoutsQuery = $payoutsTable->find('all')
                        ->where(['Payouts.user_id' => $user['id']]);

        $payoutLast = $payoutsQuery->all()->last();

        //last purchase
        $purchaseQuery = $this->Users->Purchases->find('all')
                        ->where(['Purchases.user_id' => $user['id']]);
        $purchaseData = $purchaseQuery->all()->last();

        //my subscription
        $subscriptionQuery = $this->Users->Subscriptions->find('mySubscription', ['user' => $user]);

        //check if i have a referral entry
        $referralsTable = TableRegistry::getTableLocator()->get('Referrals');
        $referralData = $referralsTable->find('all')
                        ->where(['Referrals.user_id' => $user['id']])
                        ->first();
       
        if (!is_null($referralData)) {
            # code...
            //one descendant only
            $downLine = $this->Users->Referrals->find('children', ['for' => $referralData->id,'direct' => true])
                    ->contain(['Users']);
                        
            $this->set('downLine', $downLine);
        }

                          
        

        $myReferralEarnings = TableRegistry::getTableLocator()->get('ReferralEarnings');

        $myDownline = $myReferralEarnings->find('myDownline', ['user' => $this->Users->get($user['id'])]);
        //look for unprocessed referral earnings. where default is 0
        $myDownline->where(['referral_status' => 0]);
        
        $this->set('ref_earnings', $myDownline);
       

        if (!is_null($referralData)) {
            # code...
            //referral url
            $domain = Router::url('/', true);
            $referralUrl = $domain.'users/add/?referral='.$referralData->id;

            $this->set('referralUrl', $referralUrl);

        }
        $fooAmount = 0;
        foreach($myDownline as $data){
           $fooAmount += $data->amount;
        }


        

        $this->set('count', $count);
        $this->set('earnings', $earningsSinceLastPayout);

        $this->set('user', $user);
        $this->set('payoutLast', $payoutLast);
        $this->set('purchaseLast', $purchaseData);
        $this->set('subscriptionQuery', $subscriptionQuery);
        $this->set('profileData', $profileData);
        $this->set('myLastVideosQuery', $myLastVideosQuery);
        $this->set('allPurchases', $purchaseQuery);
        $this->set('totalBal', $fooAmount + $profileData->amount);
        //$this->set('hasReferral', )
        
    }

    
    public function isAuthorized($user)
    {
        
        return true;
    }
}
