<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Packages Controller
 *
 * @property \App\Model\Table\PackagesTable $Packages
 *
 * @method \App\Model\Entity\Package[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PackagesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $results = $this->paginate($this->Packages);
        $this->set(compact('results'));

        $subscriptions = TableRegistry::getTableLocator()->get('Subscriptions');
        $user =$this->Auth->user();
        $subscriptionsQuery = $subscriptions->find('mySubscription', ['user'=>$user]);

        $this->set(compact('subscriptionsQuery',$subscriptionsQuery));
       
    }

    public function isAuthorized($user){
        $action = $this->request->getParam('action');
        
        if (in_array($action, ['index'])) {
            return true;
        }
    }

}
