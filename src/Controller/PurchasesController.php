<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Omnipay\Omnipay;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Purchases Controller
 *
 * @property \App\Model\Table\PurchasesTable $Purchases
 *
 * @method \App\Model\Entity\Purchase[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PurchasesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        
        $this->Auth->allow(['ipn']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Security->setConfig('unlockedActions', ['ipn']);
    } 

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        
        $session = $this->getRequest()->getSession();
        $user = $session->read('Auth.User');

        $user = $this->Purchases->Users->findById($user['id'])->firstorFail();

        $purchases = $this->Purchases
                    ->find('myPurchase', ['user' => $user])
                    ->where(['status' => 0, 'reference >=' => 0]);


        $this->set(compact('purchases'));
    }

    public function completeCheckout($purchaseId, $reference)
    {
        $coinpaymentsTable = TableRegistry::getTableLocator()->get('Coinpayments');
        $coinpaymentData = $coinpaymentsTable->get($reference);
        $checkoutUrl = $coinpaymentData->checkout_url;

        return $this->redirect("$checkoutUrl");
    }

    public function incomplete($packageId)
    {
        $session = $this->getRequest()->getSession();
        $user = $session->read('Auth.User');

        //check if i have an incomplete purchase
        $haveIncomplete = $this->Purchases
                                ->find('myPurchase', ['user' => $this->Purchases->Users->findById($user['id'])->firstorFail()]);
        
        $haveIncomplete->where(['status' => 0, 'reference >=' => 0]);
        $haveIncomplete->limit(1);
        
        if ($haveIncomplete->count() > 0) {
            Log::write('error', 'count greater than 0');
            

            # sth in the db. refine query . status 0 default(wating payment), reference > 0 means entry is also in coinpayments db
            
            if ($this->request->is(['post'])) {
                $proceed = $this->request->getData('proceed');
                
                if ($proceed) {
                    
                    foreach($haveIncomplete as $foobar){
                        
                        $coinpaymentsTable = TableRegistry::getTableLocator()->get('Coinpayments');
                        $coinpaymentData = $coinpaymentsTable->get($foobar->reference);
                        $checkoutUrl = $coinpaymentData->checkout_url;
                        
                    }

                    return $this->redirect("$checkoutUrl");

                }
            }
            
        }else{
            //return $this->redirect('coinpayments', $packageId);
            return $this->redirect(['action' => 'coinpayments', $packageId]);
        }
        
    }
    public function coinpayments($packageId){
        $package = $this->Purchases->Packages->get($packageId);
        $session = $this->getRequest()->getSession();
        $user = $session->read('Auth.User');
        $provider =  Configure::read('paymentProviderCoinPayment');

        

        if ($package->package_cost == 0) {
            return $this->redirect($this->referer());
        }
        if ($this->request->is(['post'])) {
            $transactPayment = $this->Purchases->startTransaction($package, $provider, $user['id']);

            if ($transactPayment) {
                
                $this->Flash->success(__('Please click checkout on your purchase below. We will notify you'));
                $this->redirect(['action' => 'index']);
            }
        }

        $this->set('packageEntity', $package);
        

    }
    public function ipn(){

        
        $this->autoRender = false;
        //update payment

        $cp_merchant_id = Configure::read('merchantId');
        $cp_ipn_secret = Configure::read('ipnSecret');
        $cp_debug_email = '';

        //These would normally be loaded from your database, the most common way is to pass the Order ID through the 'custom' POST field.

        $order_currency = 'USD';
        //$order_total = 10.00;



        function errorAndDie($error_msg) {
            //global $cp_debug_email;
            //if (!empty($cp_debug_email)) {
                $report = 'Error: '.$error_msg."\n\n";
                $report .= "POST Data\n\n";
                foreach ($_POST as $k => $v) {
                    $report .= "|$k| = |$v|\n";
                }
                //mail($cp_debug_email, 'CoinPayments IPN Error', $report);
            //}
            Log::write('error','IPN Error: '.$error_msg);
        }

        if (!isset($_POST['ipn_mode']) || $_POST['ipn_mode'] != 'hmac') {
            errorAndDie('IPN Mode is not HMAC');
        }

        if (!isset($_SERVER['HTTP_HMAC']) || empty($_SERVER['HTTP_HMAC'])) {
            errorAndDie('No HMAC signature sent.');
        }

        $request = file_get_contents('php://input');
        if ($request === FALSE || empty($request)) {
            errorAndDie('Error reading POST data');
        }

        if (!isset($_POST['merchant']) || $_POST['merchant'] != trim($cp_merchant_id)) {
            errorAndDie('No or incorrect Merchant ID passed');
        }

        $hmac = hash_hmac("sha512", $request, trim($cp_ipn_secret));
        if (!hash_equals($hmac, $_SERVER['HTTP_HMAC'])) {
        //if ($hmac != $_SERVER['HTTP_HMAC']) { <-- Use this if you are running a version of PHP below 5.6.0 without the hash_equals function
            errorAndDie('HMAC signature does not match');
        }
        
        // HMAC Signature verified at this point, load some variables.

        $txn_id = $_POST['txn_id'];
        $item_name = $_POST['item_name'];
        $item_number = $_POST['item_number'];
        $amount1 = floatval($_POST['amount1']);
        $amount2 = floatval($_POST['amount2']);
        $currency1 = $_POST['currency1'];
        $currency2 = $_POST['currency2'];
        $status = intval($_POST['status']);
        $status_text = $_POST['status_text'];




        //depending on the API of your system, you may want to check and see if the transaction ID $txn_id has already been handled before at this point
        $purchasesTable = TableRegistry::getTableLocator()->get('Purchases');
        $coinpaymentsTable = TableRegistry::getTableLocator()->get('Coinpayments');

        $checkCoinPayments = $coinpaymentsTable->find('all')
                            ->where(['transaction' => $txn_id])
                            ->limit(1);

        $coinpaymentsLocalId = '';
        foreach($checkCoinPayments as $mango){
            $coinpaymentsLocalId = $mango->id;       
        }

        //lookup purchases
        $purchasesQuery = $purchasesTable->find('all')
                        ->where(['reference' => $coinpaymentsLocalId, 'status' => 0])
                        ->limit(1);

        // Check the original currency to make sure the buyer didn't change it.
        if ($currency1 != $order_currency) {
            errorAndDie('Original currency mismatch!');
        }    
        
        // Check amount against order total
        if ($amount1 < $order_total) {
            errorAndDie('Amount is less than order total!');
        }
      
        if ($status >= 100 || $status == 2) {
            // payment is complete or queued for nightly payout, success
            $purchaseData = $purchasesQuery->firstorFail();

            $purchaseToUpdate = $purchasesTable->get($purchaseData->id);
            $purchaseToUpdate->status = 1;
            $purchasesTable->save($purchaseToUpdate);
            
        } else if ($status < 0) {
            //payment error, this is usually final but payments will sometimes be reopened if there was no exchange rate conversion or with seller consent
            $purchaseData = $purchasesQuery->firstorFail();

            $purchaseToUpdate = $purchasesTable->get($purchaseData->id);
            $purchaseToUpdate->status = 2;
            $purchasesTable->save($purchaseToUpdate);
        } else {
            //payment is pending, you can optionally add a note to the order page
            Log::write('error', 'payment is pending ');
        }
        die('IPN OK');



    }
    public function coinpaymentsTransact($packageId){
        $package = $this->Purchases->Packages->get($packageId);
        debug($package);
    }

    

    public function payMethod($packageId, $gatewayName = null){
        $package = $this->Purchases->Packages->get($packageId);

        if ($package->package_cost == 0) {
            return $this->redirect($this->referer());
        }

        $this->getRequest()->getSession()->write('packageId', $package);
        $wallets = Configure::read('WalletsAvailable');

        if (isset($gatewayName) && $gatewayName === 'perfect') {
            # redirect to perfect money
            return $this->redirect(['action' => 'perfect']);
        }

        if (isset($gatewayName) && $gatewayName === 'payeer') {
            # redirect to payeer money
            return $this->redirect(['action' => 'payeer']);
        }
       

        $this->set(compact('wallets', $wallets));
        $this->set(compact('packageId', $packageId));


    }

    public function perfect(){
        $packageEntity = $this->getRequest()->getSession()->read('packageId');

        $baseUrl = Router::url('/', true);
        
        $wallets = Configure::read('WalletsAvailable');
        $perfectMoneyArray = ($wallets['0']);
       
        $url = $perfectMoneyArray['PerfectMoney']['url'];
        $payeeAccount = $perfectMoneyArray['PerfectMoney']['PAYEE_ACCOUNT'];
        $payeeName = $perfectMoneyArray['PerfectMoney']['PAYEE_NAME'];
        $paymentUnits = $perfectMoneyArray['PerfectMoney']['PAYMENT_UNITS'];
        
        $this->set('packageEntity', $packageEntity);
        $this->set('url', $url);
        $this->set('payeeAccount', $payeeAccount);
        $this->set('payeeName', $payeeName);
        $this->set('paymentUnits', $paymentUnits);
        $this->set('baseUrl', $baseUrl);



    }

    public function payeer(){
        $packageEntity = $this->getRequest()->getSession()->read('packageId');

        $baseUrl = Router::url('/', true);
        
        $wallets = Configure::read('WalletsAvailable');
        $payeerMoneyArray = ($wallets['1']);

        $url = $payeerMoneyArray['Payeer']['url'];
        $mShop = $payeerMoneyArray['Payeer']['m_shop'];
        $mCurr = $payeerMoneyArray['Payeer']['m_curr'];
        $mKey = $payeerMoneyArray['Payeer']['m_key'];
        $mDescription = base64_encode('subscription '. $packageEntity->name.' '. $packageEntity->package_cost);
        //$mDescription = base64_encode('test');
        $mOrderId = '1';
        $mAmount = number_format($packageEntity->package_cost, 2, '.', '');
        
        
        $arHash = array(
            $mShop,
            $mOrderId,
            $mAmount,
            $mCurr,
            $mDescription,

        );

        
        $arHash[] = $mKey;
        $sign = strtoupper(hash('sha256', implode(':', $arHash)));

        $this->set('packageEntity', $packageEntity);
        $this->set('url', $url);
        $this->set('mShop', $mShop);
        $this->set('mCurr', $mCurr);
        $this->set('mKey', $mKey);
        $this->set('mDescription', $mDescription);
        $this->set('sign', $sign);
        $this->set('mAmount', $mAmount);
        $this->set('mOrderId', $mOrderId);
    }

    public function fail(){
        //simulate i bought because I can't test with real money
    }
    public function success(){

    }
    public function payeer_fail(){
        //simulate i bought because I can't test with real money
    }
    public function payeer_success(){

    }
    public function payeer_status(){

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($packageId)
    {
        $package = $this->Purchases->Packages->get($packageId);
        
        $purchase = $this->Purchases->newEntity();
        

        if ($this->request->is('post')) {
            $purchase->package_id = $packageId;
            $purchase->user_id = $this->Auth->user('id');
            $user = $this->Auth->user();
            $purchase = $this->Purchases->patchEntity($purchase, $this->request->getData());
            if ($this->Purchases->verified($purchase, $package, $user)) {
                $this->Flash->success(__('Your account has been upgraded'));

                return $this->redirect(['controller'=>'playlists','action' => 'index']);
            }
            $this->Flash->error(__('The purchase could not be saved. Please, try again.'));
        }
        
        $this->set(compact('purchase','package'));
    }
    
    public function isAuthorized($user){
        $action = $this->request->getParam('action');
        
        if (in_array($action, [
            'index',
            'payMethod',
            'fail',
            'success',
            'perfect',
            'payeer',
            'payeer_status',
            'payeer_success',
            'payeer_fail',
            'coinpayments',
            'coinpaymentsTransact',
            'incomplete',
            'completeCheckout'
            ])) {
                return true;
        }
    }
}
