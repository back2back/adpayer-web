<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ReferralEarnings Controller
 *
 * @property \App\Model\Table\ReferralEarningsTable $ReferralEarnings
 *
 * @method \App\Model\Entity\ReferralEarning[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReferralEarningsController extends AppController
{
    
    public function initialize(){
        parent::initialize();
        //$this->Auth->allow(['index','add','view','edit']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $referralEarnings = $this->paginate($this->ReferralEarnings);

        $this->set(compact('referralEarnings'));
    }

    /**
     * View method
     *
     * @param string|null $id Referral Earning id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $referralEarning = $this->ReferralEarnings->get($id, [
            'contain' => []
        ]);

        $this->set('referralEarning', $referralEarning);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $referralEarning = $this->ReferralEarnings->newEntity();
        if ($this->request->is('post')) {
            $referralEarning = $this->ReferralEarnings->patchEntity($referralEarning, $this->request->getData());
            if ($this->ReferralEarnings->save($referralEarning)) {
                $this->Flash->success(__('The referral earning has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The referral earning could not be saved. Please, try again.'));
        }
        $this->set(compact('referralEarning'));
    }

    
}
