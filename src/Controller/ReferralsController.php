<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Referrals Controller
 *
 * @property \App\Model\Table\ReferralsTable $Referrals
 *
 * @method \App\Model\Entity\Referral[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReferralsController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        ///$this->Auth->allow(['add']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
           // 'contain' => ['Users', 'ParentReferrals']
        ];
        $referrals = $this->paginate($this->Referrals);

        $this->set(compact('referrals'));
    }

    /**
     * View method
     *
     * @param string|null $id Referral id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $referral = $this->Referrals->get($id, [
            //'contain' => ['Users', 'ParentReferrals', 'ChildReferrals']
        ]);

        $this->set('referral', $referral);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($userId=null, $parentId=null)
    {
        $referral = $this->Referrals->newEntity();
        $referral->parent_id = $parentId;
        $referral->user_id = $userId;
        //if ($this->request->is('post')) {
            $referral = $this->Referrals->patchEntity($referral, $this->request->getData());
            if ($this->Referrals->save($referral)) {
                $this->Flash->success(__('The referral has been saved.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The referral could not be saved. Please, try again.'));
        //}
        $users = $this->Referrals->Users->find('list', ['limit' => 200]);
        //$parentReferrals = $this->Referrals->ParentReferrals->find('list', ['limit' => 200]);
        $this->set(compact('referral', 'users'));
    }

   
    
}
