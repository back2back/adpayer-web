<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;

/**
 * Payouts Controller
 *
 * @property \App\Model\Table\PayoutsTable $Payouts
 *
 * @method \App\Model\Entity\Payout[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PayoutsController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        //$this->Auth->allow(['add','index']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $session = $this->getRequest()->getSession();
        $user = $session->read('Auth.User');
        $this->paginate = [
            'order' => [
                'Payouts.id' => 'DESC'
            ],
            
            
        ];
        $payouts = $this->paginate($this->Payouts->findByUserId($user['id']));

        $this->set(compact('payouts'));
    }

    /**
     * View method
     *
     * @param string|null $id Payout id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $payout = $this->Payouts->get($id, [
            'contain' => []
        ]);

        $this->set('payout', $payout);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        

        $isAvail = $this->Payouts->checkAvailability();

        $session = $this->getRequest()->getSession();
        $user = $session->read('Auth.User');
        //debug($user);
        $user = $this->Payouts->Users->findById($user['id']);
        //debug($user->first());
        
        $profileData = $this->Payouts->whichProfile($user->first());

        $profileData = $profileData->first();

        //debug($profileData);
        if ($isAvail) {
            # get window details
            $windowDetail = $this->Payouts->getWindowDetails();
            
            
            //debug($windowDetail);

        }else{
            $this->Flash->error(__('You cannot withdraw at this moment'));
            return $this->redirect(['action' => 'index']);
        }

        //check my earnings from referrals
        
        $myReferralEarnings = TableRegistry::getTableLocator()->get('ReferralEarnings');

        $myDownline = $myReferralEarnings->find('myDownline', ['user' => $user->first()]);
        //look for unprocessed referral earnings. where default is 0
        $myDownline->where(['referral_status' => 0]);
        
        $this->set('ref_earnings', $myDownline);
        $mainBalance = $profileData->amount;
        $this->set('main_balance', $mainBalance);

        //do i Have a payout method?
        //debug($profileData->btc_address);
        if (is_null($profileData->btc_address) || !$profileData->btc_address || empty($profileData->btc_address)) {
            $this->set('noBTCAddress', true);
        }else{
            $this->set('noBTCAddress', false);
        }

        $payout = $this->Payouts->newEntity();
        if ($this->request->is('post')) {
            
            $cumulativeRef = 0;
            foreach($myDownline as $refData)
                    {
                        $myReferrals = $refData->amount;
                        $cumulativeRef += $myReferrals;
                    } 

            $payout->amount = $mainBalance + $cumulativeRef;
            $payout->user_id = $profileData->user_id;
            $payout->profile_id = $profileData->id;
            $payout->btc_address = $profileData->btc_address;
            $payout->window_key = $windowDetail->window_key;


            

            $payout = $this->Payouts->patchEntity($payout, $this->request->getData());
            
            if ($this->Payouts->save($payout)) {
                $this->Flash->success(__('The payout has been saved.'));
                //set the account balance to zero.
                $setBalance = $this->Payouts->payoutData($profileData->amount, $profileData->id);

                //set referral balance to zero
                $refBalanceToZero = $this->Payouts->resetReferralBal($myDownline, $payout->id);

                

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The payout could not be saved. Please, try again.'));
        }
        $this->set(compact('payout'));
    }

    

    public function isAuthorized($user){
        $action = $this->request->getParam('action');
        if (in_array($action, ['index', 'add'])) {
            return true;
        }
    }
}
