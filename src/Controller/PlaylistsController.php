<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Client;
use Cake\Routing\Router;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\I18n\Date;


/**
 * Playlists Controller
 *
 * @property \App\Model\Table\PlaylistsTable $Playlists
 *
 * @method \App\Model\Entity\Playlist[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PlaylistsController extends AppController
{
    public $videoData;

    public function initialize()
    {
        parent::initialize();
        
    }
    public function beforeFilter(Event $event)
    {
         //this line is not necessary if you pass the _csrfToken
         
         //$this->Security->setConfig('unlockedActions', ['watch','addToWatched']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $random = array('racing','movies','games','river','beauty','art','sports','football','phone','celebrity','hollywood','music');
        $sizeRandom = count($random);

        $today = Date::today();
        $lastMonth = $today->subMonth();
        $lastMonth = $lastMonth->toUnixString();
        

        $bar = mt_rand(0, $sizeRandom-1);
        //debug($bar);
        $http = new Client();
        $response = $http
        ->get("https://api.dailymotion.com/videos?country=US&search=$random[$bar]&fields=duration,id,thumbnail_240_url,owner.screenname,title&limit=27&created_after=$lastMonth&related_videos_algorithm=uploader-only");
        $response = $response->getJson();


        $this->videoData = $response;

        $this->paginate = [
            'contain' => ['Users']
        ];
        $playlists = $this->paginate($this->Playlists);

        $this->set(compact('playlists'));
        $this->set('response', $response);
        //print_r($response);

        $grids = array('grid-1','grid-2','grid-3','grid-4','grid-5','grid-6','grid-7','grid-8','grid-9','grid-10','grid-11','grid-12');
        $this->set('grids', $grids);

    }

    /**
     * View method
     *
     * @param string|null $id Playlist id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $playlist = $this->Playlists->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('playlist', $playlist);
    }

    public function watch($dailymotionVideo){
        
        $http = new Client();
        $response = $http->get('https://api.dailymotion.com/video/'.$dailymotionVideo.'?fields=description,title');
        $response = $response->getJson();
        $this->set('response', $response);

        //related videos
        $related = $http->get('https://api.dailymotion.com/video/'.$dailymotionVideo.'/related?fields=duration,id,thumbnail_120_url,owner.screenname,title&limit=5');
        $related = $related->getJson();
        $this->set('dailymotionVideo', $dailymotionVideo);
        $this->set('related', $related);

        $domain = Router::url('/', true);
        $playlistUrl = $domain.'api/playlists/add-to-watched/'.$dailymotionVideo.'.json';
        $this->set('playlistUrl', $playlistUrl);

        $videoTimeout = Configure::read('videoTimeout');
        $this->set('timeout', $videoTimeout);

        $isLegit = $this->Playlists->isVideoLegit($this->Auth->user('id'));
        $this->set('legit', $isLegit);
        
    }

    

    public function addToWatched($dailymotionVideo)
    {
        
        
           
            $this->request->allowMethod(['post', 'put']);

            $playlist = $this->Playlists->newEntity();
            $playlist->user_id = $this->Auth->user('id');
            $playlist->video_ref = $this->request->getData('video_ref');
            if ($this->Playlists->save($playlist)) {
                $message = 'Saved';
            } else {
                $message = 'Error';
            }
            $user = $this->Auth->user('id');
            $this->set([
                'message' => $message,
                'playlist' => $playlist,
                'user' => $user,
                '_serialize' => ['message', 'playlist','user']
            ]);    

    }

    public function isAuthorized($user){
        $action = $this->request->getParam('action');
        if (in_array($action, ['index', 'addToWatched', 'watch'])) {
            return true;
        }
    }

    

    

    
}
