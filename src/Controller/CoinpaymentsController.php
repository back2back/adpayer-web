<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Coinpayments Controller
 *
 * @property \App\Model\Table\CoinpaymentsTable $Coinpayments
 *
 * @method \App\Model\Entity\Coinpayment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CoinpaymentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
        ];
        $coinpayments = $this->paginate($this->Coinpayments);

        $this->set(compact('coinpayments'));
    }

    /**
     * View method
     *
     * @param string|null $id Coinpayment id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $coinpayment = $this->Coinpayments->get($id, [
            'contain' => ['Users'],
        ]);

        $this->set('coinpayment', $coinpayment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $coinpayment = $this->Coinpayments->newEntity();
        if ($this->request->is('post')) {
            $coinpayment = $this->Coinpayments->patchEntity($coinpayment, $this->request->getData());
            if ($this->Coinpayments->save($coinpayment)) {
                $this->Flash->success(__('The coinpayment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The coinpayment could not be saved. Please, try again.'));
        }
        $users = $this->Coinpayments->Users->find('list', ['limit' => 200]);
        $this->set(compact('coinpayment', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Coinpayment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $coinpayment = $this->Coinpayments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $coinpayment = $this->Coinpayments->patchEntity($coinpayment, $this->request->getData());
            if ($this->Coinpayments->save($coinpayment)) {
                $this->Flash->success(__('The coinpayment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The coinpayment could not be saved. Please, try again.'));
        }
        $users = $this->Coinpayments->Users->find('list', ['limit' => 200]);
        $this->set(compact('coinpayment', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Coinpayment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $coinpayment = $this->Coinpayments->get($id);
        if ($this->Coinpayments->delete($coinpayment)) {
            $this->Flash->success(__('The coinpayment has been deleted.'));
        } else {
            $this->Flash->error(__('The coinpayment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
