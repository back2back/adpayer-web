<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Subscriptions Controller
 *
 * @property \App\Model\Table\SubscriptionsTable $Subscriptions
 *
 * @method \App\Model\Entity\Subscription[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SubscriptionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $user = $this->Auth->user();
        $subscriptions = $this->Subscriptions->find('all')
                        ->where(['Subscriptions.user_id' => $user['id']])
                        ->contain(['Packages'])
                        ->limit(1);

        

        $this->set(compact('subscriptions'));
    }

    /**
     * View method
     *
     * @param string|null $id Subscription id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subscription = $this->Subscriptions->get($id, [
            'contain' => ['Packages', 'Users']
        ]);

        $this->set('subscription', $subscription);
    }



    

    public function isAuthorized($user){
        $action = $this->request->getParam('action');
        
        if (in_array($action, ['index'])) {
            return true;
        }
        if (in_array($action, ['view'])) {
            $subscriptionId = $this->request->getParam('pass.0');
            if (!$subscriptionId) {
                return false;
            }
            $subscription = $this->Subscriptions->findById($subscriptionId)->firstOrFail();
            return $subscription->user_id === $user['id'];
        }
    }
}
