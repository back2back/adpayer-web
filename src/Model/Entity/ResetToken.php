<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ResetToken Entity
 *
 * @property int $id
 * @property string $token
 * @property string $email
 * @property int|null $is_valid
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $expires
 */
class ResetToken extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'token' => true,
        'email' => true,
        'is_valid' => true,
        'created' => true,
        'modified' => true,
        'expires' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token'
    ];
}
