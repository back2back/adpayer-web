<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Text;

/**
 * User Entity
 *
 * @property string $id
 * @property string $username
 * @property string|null $profile_id
 * @property string|null $package_id
 * @property string|null $f_name
 * @property string|null $l_name
 * @property string $password
 * @property bool|null $is_admin
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Profile $profile
 * @property \App\Model\Entity\Package $package
 * @property \App\Model\Entity\Playlist[] $playlists
 * @property \App\Model\Entity\Purchase[] $purchases
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'profile_id' => true,
        'package_id' => true,
        'f_name' => true,
        'l_name' => true,
        'password' => true,
        'created' => true,
        'modified' => true,
        'profile' => true,
        'package' => true,
        'playlists' => true,
        'purchases' => true,
        'email' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($value);
        }
    }
   
}
