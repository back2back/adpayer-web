<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReferralEarning Entity
 *
 * @property int $id
 * @property string $parent
 * @property string $child
 * @property float|null $amount
 * @property bool|null $referral_status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class ReferralEarning extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'parent' => true,
        'child' => true,
        'amount' => true,
        'referral_status' => true,
        'created' => true,
        'modified' => true
    ];
}
