<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Security;
use Cake\Core\Configure;
use Cake\Log\Log;

/**
 * Profile Entity
 *
 * @property string $id
 * @property string $user_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property string|null $payeer_ref
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User[] $users
 */
class Profile extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'user_id' => true,
        'created' => true,
        'btc_address' => true,
        'modified' => true,
        'users' => true
    ];

    
}
