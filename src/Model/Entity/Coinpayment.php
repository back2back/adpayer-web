<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Coinpayment Entity
 *
 * @property int $id
 * @property float $amount
 * @property string $transaction
 * @property int $confirms_needed
 * @property int $timeout
 * @property string $checkout_url
 * @property string $status_url
 * @property string $qrcode_url
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property string $user_id
 *
 * @property \App\Model\Entity\User $user
 */
class Coinpayment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'amount' => true,
        'transaction' => true,
        'confirms_needed' => true,
        'timeout' => true,
        'checkout_url' => true,
        'status_url' => true,
        'qrcode_url' => true,
        'created' => true,
        'modified' => true,
        'user_id' => true,
        'user' => true,
    ];
}
