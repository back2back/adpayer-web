<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Window Entity
 *
 * @property int $id
 * @property int $window_status
 * @property string $window_key
 * @property \Cake\I18n\FrozenTime|null $start_time
 * @property \Cake\I18n\FrozenTime|null $end_time
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $user_id
 * @property float|null $amt
 *
 * @property \App\Model\Entity\User $user
 */
class Window extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'window_status' => true,
        'window_key' => true,
        'start_time' => true,
        'end_time' => true,
        'created' => true,
        'modified' => true,
        'user_id' => true,
        'amt' => true,
        'user' => true
    ];
}
