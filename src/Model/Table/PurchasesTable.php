<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Utility\Text;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;
use Omnipay\Omnipay;
use Cake\Http\Client;
use Cake\Core\Configure;
use Cake\Event\Event;

use Cake\Routing\Router;

/**
 * Purchases Model
 *
 * @property \App\Model\Table\PackagesTable&\Cake\ORM\Association\BelongsTo $Packages
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Purchase get($primaryKey, $options = [])
 * @method \App\Model\Entity\Purchase newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Purchase[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Purchase|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Purchase saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Purchase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Purchase[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Purchase findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */

//status : 0 = waiting payment 1 = paid 2 = failed
class PurchasesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('purchases');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Packages', [
            'foreignKey' => 'package_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->decimal('amount')
            ->allowEmptyString('amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['package_id'], 'Packages'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }



    public function verified($upgradedPackage = null, $user = null)
    {   
        $packageDuration = $upgradedPackage->expires_in_months;
        $upgradedPackageId = $upgradedPackage->id;


        
        //get my subscriptions
        $subscriptions = TableRegistry::getTableLocator()->get('Subscriptions');
        $query = $subscriptions->find('MySubscription', ['user' => $user]);

        foreach($query as $dataRow){
            $subscriptionId = $dataRow->id;
            //edit this
           
            $timeNow = new Time('now');

            $dueDate = new Time('now');
            $dueDate = $dueDate->addMonth($packageDuration);

            $data = [
                'package_id' => $upgradedPackageId,
                'begin_date' => $timeNow,
                'end_date' => $dueDate];

            $newSubscriptionData = $subscriptions->patchEntity($dataRow, $data);
            if ($subscriptions->save($newSubscriptionData)) {
             
                //$this->save($purchase);
               return true;
            }else{
                return false;
            }

        }
        
    }

    public function findMyPurchase(Query $query, array $options)
    {
        $user = $options['user'];

        return $query->where(['user_id' => $user->id]);
    }

    public function startTransaction($package, $paymentProvider,$userId)
    {
        $packageId =  $package->id;
        $packageAmount = $package->package_cost;
        $packageName = $package->name;
        $conn = ConnectionManager::get('default');

        $conn->begin();
        //add to purchases Table
        $purchasesEntity = $this->newEntity();
        $purchasesEntity->amount = $packageAmount;
        $purchasesEntity->package_id = $packageId;
        $purchasesEntity->provider = $paymentProvider;
        $purchasesEntity->user_id = $userId;



        $this->save($purchasesEntity);
        //the new purchase id 
        $purchaseId = $purchasesEntity->id;

        $userEntityForEmail = $this->Users->findById($userId)->firstOrFail();

        $domain = Router::url('/', true);
            
        $ipnUrl = $domain.'purchases/ipn';
        
        

        //got to coinpayments api
            $gateway = Omnipay::create('Coinpayments');
            $gateway->initialize(array(
                'publicKey' => Configure::read('coinPaymentsPublicKey'),
                'privateKey' => Configure::read('coinPaymentsPrivateKey')
            ));

            $response = $gateway->transaction([
                'amount' => $packageAmount,
                'currency1' => 'USD',
                'currency2' => Configure::read('primaryCoin'),
                //'address' => '', // leave blank send to follow your settings on the Coin Settings page
                'buyer_email' => $userEntityForEmail->email,
                'item_name' => $packageName,
                'ipn_url' => $ipnUrl,
            ])->send();

            if ($response->isSuccessful()) {
                $data = $response->getData(); 

                $ourData = array();
                foreach($data as $bar){

                    if (is_array($bar)) {
                        //$this->writeToCoinpayments($bar);
                        foreach($bar as $kk){
                            array_push($ourData, $kk);
                            //Log::write('error', 'pushing '. $kk);
                        }
                    }
               }

              
                $transCoinAmount = $ourData[0];
                $transId = $ourData[1];
                $address = $ourData[2];
                $confirms = $ourData[3];;
                $timeout = $ourData[4];
                $checkout = $ourData[5];
                $status = $ourData[6];
                $qr = $ourData[7];

                $coinpayments = TableRegistry::getTableLocator()->get($paymentProvider);

                $data = $coinpayments->newEntity();
                $data->amount = $transCoinAmount;
                $data->transaction = $transId;
                $data->address = $address;
                $data->confirms_needed = $confirms;
                $data->timeout = $timeout;
                $data->checkout_url = $checkout;
                $data->status_url = $status;
                $data->qrcode_url = $qr;
                $data->user_id = $userId;

                if ($coinpayments->save($data)) {
                    $coinpaymentsId = $data->id;
                    //save this is to this table
                    
                    $whichPurchaseEntity = $this->findById($purchaseId)->firstOrFail();
                    
                    $whichPurchaseEntity->reference = $coinpaymentsId;
                    $this->save($whichPurchaseEntity);
                }
                
                $conn->commit();
                return true;
            }else{
                $data = $response->getData(); 
                Log::write('error','fail coinpayments '. $data);
                $conn->rollback();
            }

            return false;
    }

   

    public function beforeSave($event, $entity, $options)
    {
        if ($entity->isNew()) {
            $packageId = Text::uuid();
            $entity->id = $packageId;

            return true;
        }
    }

    public function afterSave($event, $entity, $options)
    {
        
        if (!$entity->isNew()) {
            
            $status = $entity->status;
            if ($status == 1) {
                //when status is paid
                //send an email. upgrade the package
                
                $packageEntity = $this->Packages->get($entity->package_id);
                $userId = $entity->user_id;
                $user = $this->Users->findById($userId)->firstOrFail();
                if($this->verified($packageEntity, $user))
                {
                    Log::write('error', 'Subscription upgraded');
                    //fire event
                    $event = new Event('Model.Email.PurchaseConfirmed', $this, [
                        'purchase' => $entity]);
                    $this->getEventManager()->dispatch($event);
                }
            }

            if ($status == 0) {
                # purchase began send email instructions to pay offsite
                $data = $this->get($entity->id);
                
                $providerNameTable = $data->provider;

                $providerTable = TableRegistry::getTableLocator()->get($providerNameTable);
                $paymentEntity = $providerTable->get($data->reference);


                    //fire event
                    $event = new Event('Model.Email.PurchaseBegan', $this, [
                        'payment' => $paymentEntity]);
                    $this->getEventManager()->dispatch($event);


            }
        }
    }
    
}
