<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Log\Log;


/**
 * ReferralEarnings Model
 *
 * @method \App\Model\Entity\ReferralEarning get($primaryKey, $options = [])
 * @method \App\Model\Entity\ReferralEarning newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ReferralEarning[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ReferralEarning|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReferralEarning saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ReferralEarning patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ReferralEarning[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ReferralEarning findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReferralEarningsTable extends Table 
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('referral_earnings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('parent')
            ->maxLength('parent', 255)
            ->requirePresence('parent', 'create')
            ->notEmptyString('parent');

        $validator
            ->scalar('child')
            ->maxLength('child', 255)
            ->requirePresence('child', 'create')
            ->notEmptyString('child');

        $validator
            ->decimal('amount')
            ->allowEmptyString('amount');

        $validator
            ->boolean('referral_status')
            ->allowEmptyString('referral_status');

        return $validator;
    }

    public function findMyDownline(Query $query, array $options )
    {
        $parentId = $options['user'];
        return $query ->where(['parent' => $parentId->id]);
    }

    
}
