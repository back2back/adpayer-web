<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


/**
 * Users Model
 *
 * @property \App\Model\Table\ProfilesTable&\Cake\ORM\Association\BelongsTo $Profiles
 
 * @property \App\Model\Table\PlaylistsTable&\Cake\ORM\Association\HasMany $Playlists
 * @property \App\Model\Table\PurchasesTable&\Cake\ORM\Association\HasMany $Purchases
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Profiles', [
            'foreignKey' => 'profile_id'
        ]);
        
        $this->hasMany('Playlists', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Purchases', [
            'foreignKey' => 'user_id'
        ]);

        $this->hasOne('Profiles', [
            'foreignKey' => 'user_id'
        ]);

        $this->belongsTo('Subscriptions', [
            'foreignKey' => 'subscription_id'
        ]);
        $this->hasMany('Payouts', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Referrals', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('id')
            ->maxLength('id', 255)
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('email')  
            ->requirePresence('email', 'create')
            ->notEmptyString('email')
            ->email('email')
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'E-mail must be valid'
            ])
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('username')
            ->maxLength('username', 16)
            ->requirePresence('username', 'create')
            ->notEmptyString('username')
            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('f_name')
            ->maxLength('f_name', 16)
            ->requirePresence('f_name');
            

        $validator
            ->scalar('l_name')
            ->maxLength('l_name', 16)
            ->requirePresence('l_name');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->boolean('is_admin')
            ->allowEmptyString('is_admin');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->existsIn(['profile_id'], 'Profiles'));
        $rules->add($rules->existsIn(['subscription_id'], 'Subscriptions'));

        return $rules;
    }

    public function beforeSave($event, $entity, $options){
        if ($entity->isNew()) {
            $userId = Text::uuid();
            $entity->id = $userId;
            
            //get packages
            $packages = TableRegistry::getTableLocator()->get('Packages');
            $query = $packages
                ->find()
                ->where(['is_default' => 1])
                ->first();
            $packageId = $query->id;
            $packageDuration = $query->expires_in_months;

            $timeNow = new Time('now');

            $dueDate = new Time('now');
            $dueDate = $dueDate->addMonth($packageDuration);

            $subscriptionsTable = TableRegistry::getTableLocator()->get('Subscriptions');
            $subscription= $subscriptionsTable->newEntity([
                'package_id' => $packageId,
                'begin_date' => $timeNow,
                'end_date' => $dueDate,
                'user_id' => $userId,
                'created' => $timeNow,
                'modified' => $timeNow



            ]);

            //save subscription
            $subscriptionsTable->save($subscription);
            

            $profileId = Text::uuid();
            $profilesTable = TableRegistry::getTableLocator()->get('Profiles');
            $profile = $profilesTable->newEntity([
                'user_id' => $userId,
                'id' => $profileId
            ]);
            //debug($profile);

            $profilesTable->save($profile);

            //back to user table
            $entity->profile_id = $profileId;
            $entity->subscription_id = $subscription->id;
            //debug($entity);
            return true;
        }

    }

    public function afterSave($event,$entity,$options){
        $referralTable = TableRegistry::getTableLocator()->get('Referrals');
        $referral = $referralTable->newEntity();
        $referral->user_id = $entity->id;
        $referralLink = $entity->referralId;
        if (isset($referralLink)) {
            $referral->parent_id = $referralLink;
        }else{
            $referral->parent_id = 0;
        }

        $referralTable->save($referral);
        
    }

    


}
