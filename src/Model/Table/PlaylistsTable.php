<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
/**
 * Playlists Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Playlist get($primaryKey, $options = [])
 * @method \App\Model\Entity\Playlist newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Playlist[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Playlist|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Playlist saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Playlist patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Playlist[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Playlist findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PlaylistsTable extends Table
{
    public $earningsToDb = 0.00;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('playlists');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('video_vendor')
            ->maxLength('video_vendor', 16)
            ->allowEmptyString('video_vendor');

        $validator
            ->scalar('video_ref')
            ->maxLength('video_ref', 64)
            ->allowEmptyString('video_ref');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function findMyVideosSinceLastPayout(Query $query, array $options){
        $user = $options['user'];
        //last payout
        $payoutsTable = TableRegistry::getTableLocator()->get('Payouts');
        $payoutsQuery = $payoutsTable->find('all')
                        ->where(['Payouts.user_id' => $user->id]);

        $row = $payoutsQuery->all()->last();
        

        if (empty($row)) {
            # no previous payouts
            $query = $this->find()
                ->where(['Playlists.user_id' => $user['id']]);

        }else{
            $query = $this->find()
                    ->where(array(['Playlists.user_id' => $user['id']], ['Playlists.created >=' => $row->created]));
        }
        
        return $query;
                
    }

    public function checkVideoCount24hrs($user){
        //within last 24 hours
        $time = Time::now();

        $time24hrsAgo = date('Y-m-d H:i:s', strtotime('-24 hours'));
        

        $total = $this->find()
            ->where(['user_id' => $user, 'created >=' => $time24hrsAgo])
            ->count(); 

        return $total;
    }

    public function isVideoLegit($user){
        $count = $this->checkVideoCount24hrs($user);

        $subscriptionsTable = TableRegistry::getTableLocator()->get('Subscriptions');

        $mySubscriptionQuery = $subscriptionsTable->find('all')
                                ->where(['Subscriptions.user_id' => $user])
                                ->contain('Packages')
                                ->limit(1);
        $maxAllowedVideos = 0;
        $earnings = 0;
        foreach($mySubscriptionQuery as $data){

            $maxAllowedVideos = $data->package->video_load;
            $earnings = $data->package->cost_per_video;




        }
        if ($count >= $maxAllowedVideos) {
            //dont save
            return false;
            $this->earningsToDb = 0.00;
        }else{
            $this->earningsToDb = $earnings;
            return true;
        }
    }
    public function beforeSave($event, $entity, $options){
        $user = $entity->user_id;
        //check count of any videos in db
        if ($entity->isNew()) {
            
            if ($this->isVideoLegit($user)) {
                $entity->earn = $this->earningsToDb;
               return true;
            }else{
                return false;
            }
            
        }
        
    }

    public function afterSave($event, $entity, $options){
        $userId = $entity->user_id;
        
        $profilesTable = TableRegistry::getTableLocator()->get('Profiles');

        $profilesQuery = $profilesTable->find('all')
                        ->where(['Profiles.user_id' => $userId])
                        ->limit(1);
        
        $currentAmount = 0.00;
        $adjustedAmount = 0.00;

        foreach($profilesQuery as $dataRow){
            $profileId = $dataRow->id;
            $currentAmount = $dataRow->amount;

            $adjustedAmount = $currentAmount + $this->earningsToDb;

            $prof = $profilesTable->get($profileId);
            $prof->amount = $adjustedAmount;

     
            $profilesTable->save($prof);

        }
        //earnings from video to got to referral earnings.
        // Event
        $event = new Event('Model.Playlist.afterSave', $this,[
            'playlist' => $entity]);
        $this->getEventManager()->dispatch($event);
        Log::write('error', 'The playlist event has been dispatched');

    }
}
