<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Coinpayments Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Coinpayment get($primaryKey, $options = [])
 * @method \App\Model\Entity\Coinpayment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Coinpayment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Coinpayment|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Coinpayment saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Coinpayment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Coinpayment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Coinpayment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CoinpaymentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('coinpayments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->decimal('amount')
            ->requirePresence('amount', 'create')
            ->notEmptyString('amount');

        $validator
            ->scalar('transaction')
            ->maxLength('transaction', 255)
            ->requirePresence('transaction', 'create')
            ->notEmptyString('transaction')
            ->add('transaction', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('confirms_needed')
            ->requirePresence('confirms_needed', 'create')
            ->notEmptyString('confirms_needed');

        $validator
            ->integer('timeout')
            ->requirePresence('timeout', 'create')
            ->notEmptyString('timeout');

        $validator
            ->scalar('checkout_url')
            ->maxLength('checkout_url', 255)
            ->requirePresence('checkout_url', 'create')
            ->notEmptyString('checkout_url');

        $validator
            ->scalar('status_url')
            ->maxLength('status_url', 255)
            ->requirePresence('status_url', 'create')
            ->notEmptyString('status_url');

        $validator
            ->scalar('qrcode_url')
            ->maxLength('qrcode_url', 255)
            ->requirePresence('qrcode_url', 'create')
            ->notEmptyString('qrcode_url');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['transaction']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
