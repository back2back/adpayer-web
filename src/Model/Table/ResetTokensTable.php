<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

use Cake\Log\LogTrait;
use Cake\Event\Event;

use Cake\Mailer\Email;
use Cake\Core\Configure;
use Cake\Routing\Router;


/**
 * ResetTokens Model
 *
 * @method \App\Model\Entity\ResetToken get($primaryKey, $options = [])
 * @method \App\Model\Entity\ResetToken newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ResetToken[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ResetToken|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ResetToken saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ResetToken patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ResetToken[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ResetToken findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ResetTokensTable extends Table
{
    use LogTrait;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('reset_tokens');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    public function findToken(Query $query, array $options){
        $token = $options['token'];

        $data = $query
            ->where(['token' => $token, 'is_valid' => 1])
            ->last();


        return $data;
    }

    public function updateToken($data, $updated){
        
        //is valid = 0
        //load users table update

        $query = $this->find()
                ->where(['token' => $data->token]);

        $rowData = $query->first();
        $rowData->is_valid = 0;

        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $queryUser = $usersTable->findByEmail($data->email);
        
        $foo = $queryUser->first();
        //debug($foo);
        $userData = $usersTable->get($foo->id);
        $userData->password = $updated;

        //debug($updated);
        if ($this->save($rowData) && $usersTable->save($userData)) {


            
            # code...
            return true;
        }
        //$this->save($rowData);
        //$usersTable->save($userData)
        //return true;
        




    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

       

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->allowEmptyString('is_valid');

        $validator
            ->dateTime('expires')
            ->allowEmptyDateTime('expires');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->isUnique(['email']));

        return $rules;
    }
    public function beforeSave($event, $entity, $options){
        if ($entity->isNew()) {
            //check if email exists
            $usersTable = TableRegistry::getTableLocator()->get('Users');
            $queryUser = $usersTable->findByEmail($entity->email);
            
            $user = $queryUser->firstOrFail();

            if (!$user) {
               return false;
            }

            $token = Text::uuid();
            $entity->token = $token;
            $expires = Time::now();
            $other = $expires->addDays(1);
            $entity->expires = $other;
            return true;
        }
    }

    public function afterSave($event, $entity, $options){
        if ($entity->isNew()) {

            $usersTable = TableRegistry::getTableLocator()->get('Users');
            $queryUser = $usersTable->findByEmail($entity->email);
            
            $user = $queryUser->firstOrFail();

            
            $event = new Event('Model.Email.ResetToken', $this, [
                        'reset' => $entity]);
                    $this->getEventManager()->dispatch($event);
        }

    }
}
