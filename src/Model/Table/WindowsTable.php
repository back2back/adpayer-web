<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;
use Cake\I18n\Time;
use Cake\Log\Log;

/**
 * Windows Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Window get($primaryKey, $options = [])
 * @method \App\Model\Entity\Window newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Window[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Window|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Window saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Window patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Window[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Window findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WindowsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('windows');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Payouts', [
            'foreignKey' => 'window_key'
        ]);

       

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

       /* $validator
            ->notEmptyString('window_status');

        $validator
            ->scalar('window_key')
            ->maxLength('window_key', 255)
            ->requirePresence('window_key', 'create')
            ->notEmptyString('window_key');*/

        $validator
            ->dateTime('start_time')
            ->allowEmptyDateTime('start_time');

        $validator
            ->dateTime('end_time')
            ->allowEmptyDateTime('end_time');

        $validator
            ->decimal('amt')
            ->allowEmptyString('amt');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function findAnyWindow(Query $query){
        $query = $this->find('all');
        return $query;
    }

    public function findOpenWindows(Query $query ){
        //get open window. That's active. 

        $now = new Time('now');
        //debug($now);
        //$query->where(['end_time >'  =>$now]);
        $query->where(['window_status' => 1]);
        return $query;
        
    }

    public function beforeSave($event, $entity, $options){

        $anyWindow = $this->find('anyWindow');
        //debug($anyWindow->last());

        $anyWindow = $anyWindow->last();
        if (is_null($anyWindow)) {
            # there are no previous windows. Create one
            Log::write('error' ,'Creating a new window. No previous windows in db');
            if ($entity->isNew()) {
                $windowKey = Text::uuid();
                $entity->window_key = $windowKey;

                $now = new Time('now');
                $entity->start_time = $now;

                $kesho = new Time('now');
                $expires = $kesho->addDay(1);
                $entity->end_time = $expires;

                return true;
            }
        }


        //there's a window. get the last one

        $allWindowsQuery = $this->find('openWindows');
        
        $all = $allWindowsQuery->last();
        
        $lastWindowStatus = $all->window_status;
        if ( $entity->isNew() && $lastWindowStatus) {
            Log::write('error', 'Theres an open window please close it first');
            return false;
        }else{
            if ($entity->isNew()) {
                $windowKey = Text::uuid();
                $entity->window_key = $windowKey;

                $now = new Time('now');
                $entity->start_time = $now;

                $kesho = new Time('now');
                $expires = $kesho->addDay(1);
                $entity->end_time = $expires;

                return true;
            }
        }
        
        
    }
}
