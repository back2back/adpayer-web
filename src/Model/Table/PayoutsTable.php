<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

use Cake\Http\Client;

/**
 * Payouts Model
 *
 * @method \App\Model\Entity\Payout get($primaryKey, $options = [])
 * @method \App\Model\Entity\Payout newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Payout[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Payout|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Payout saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Payout patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Payout[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Payout findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PayoutsTable extends Table
{
    protected $_userFoo;
    protected $_profileFoo;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payouts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Windows', [
            'foreignKey' => 'window_key'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('id')
            ->maxLength('id', 16)
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->decimal('amount')
            ->allowEmptyString('amount');



        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->isUnique(['user_id','window_key']));

        return $rules;
    }

    public function checkAvailability(){
        $openWindowsQuery = $this->Windows->find('openWindows');
        $last = $openWindowsQuery->last();
        
        if (is_null($last)) {
            return false;
        }
        if ($last->window_status) {
            # open window avail...
            return true;
        }else{
            return false;
        }
        
    }
    public function getWindowDetails(){
        $openWindowsQuery = $this->Windows->find('openWindows');
        $last = $openWindowsQuery->last();
        
        return $last;
        
    }
    public function whichProfile($data ){
        $profiles = TableRegistry::getTableLocator()->get('Profiles');
        $profile =  $profiles->find('myProfile', ['user' => $data]);
        
        $this->userFoo = $data;
        $this->profileFoo = $profile->first();
        
        /*if($this->minimumWithdraw($profile)){

            return $profile;
        } */
        return $profile;


    }
    public function payoutData($amount, $profileId){
        //$data = array($amount, $userId);
        //return $data;
        $profilesTable = TableRegistry::getTableLocator()->get('Profiles');
        $profile = $profilesTable->get($profileId);
        $currentAmount = $profile->amount;
        $afterWithdraw = $currentAmount - $amount;
        //debug($afterWithdraw);
        $profile->amount = $afterWithdraw;
        if ($profilesTable->save($profile)) {
            
            return true;
        }
    }
    public function resetReferralBal(Query $query, $payoutId){
        $refEarningsTable = TableRegistry::getTableLocator()->get('ReferralEarnings');
        
            foreach($query as $refData)
                    {
                        $id = $refData->id;
                        $refEntity = $refEarningsTable->get($id);
                        $refEntity->payout_id = $payoutId;
                        $refEntity->referral_status = 1;
                        $refEarningsTable->save($refEntity);
                        
                    } 
    }

    public function minimumWithdraw($profile){
        $subscriptionTable = TableRegistry::getTableLocator()->get('Subscriptions');
        $data = $subscriptionTable->find('mySubscription', ['user' => $this->userFoo])->first();

        $package = $data->package;
        
        if ($package->min_payout > $profile->amount) {
            //not enough for payout set
            Log::write('error', 'did not reach threshold');
            return false;
        }else{
            Log::write('error', 'reached threshold');
            return true;
        }
        
    }
    public function beforeSave($event, $entity, $options){
        if ($entity->isNew()) {
            //check window
            $isAvail =  $this->checkAvailability();

            if ($isAvail) {
                # code...
                //check balance
                //debug($entity);
                //return false;
                return $this->minimumWithdraw($this->profileFoo);


            }
            

            return false;
        }
    }

    public function afterSave($event, $entity, $options){
        if ($entity->isNew()) {
            
            $http = new Client();

            $response = $http->get('https://blockchain.info/tobtc', ['currency' => 'USD', 'value' => $entity->amount]);
            if ($response->getJson()) {
                $entity->btc_amount = $response->getJson();
            }else{
                $entity->btc_amount = 0.0;
            }

            $this->save($entity);
            
            
        }
    }
}
