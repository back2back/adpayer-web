<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<!-- <div class="callout success" data-closable><?= $message ?></div>
 -->
<div data-closable class="alert-box callout success">
  <i class="fa fa-check"></i> <?= $message ?>
  <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
    <span aria-hidden="true">&CircleTimes;</span>
  </button>
</div>