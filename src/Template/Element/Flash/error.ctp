<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<!-- <div class="callout error" onclick="this.classList.add('hidden');"><?= $message ?></div>
 -->
 <div data-closable class="alert-box callout alert">
  <i class="fa fa-ban"></i> <?= $message ?>
  <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
    <span aria-hidden="true">&CircleTimes;</span>
  </button>
</div>