<?php
?>
<div class="grid-x">
	<div class="cell large-12">
		<div id="invoice">
			<div class="cardify invoice">
				<h3><?= __('Purchase Order')?></h3>
				<div class="grid-x">
					<div class="cell small-6">
						<h6 class="invoice-header"><?= __('Item name')?></h6>
						<p><?= $packageEntity->name?></p>
					</div>
					<div class="cell small-6">
						<h6 class="invoice-header"><?= __('Amount')?></h6>
						$ <?= $packageEntity->package_cost; ?>
					</div>
				<hr>
				</div>


			</div>
		</div>
	</div>
</div>


<!-- <h3><?= __('Confirm order')?></h3>
		<p><?= __('You are about to pay')?></p>
		<p><?= $packageEntity->name; ?></p>
		<p><?PHP echo 'USD'.' '.$packageEntity->package_cost; ?></p> -->