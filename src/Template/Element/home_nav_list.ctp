<?php
?>

<ul>
  <li><a href="/">Home</a></li>
  <li><?= $this->Html->link(__('Articles'),['controller' => 'Articles','action' => 'index']) ?></li>
  <li><?= $this->Html->link(__('About Us'), ['controller' => 'Pages', 'action' => 'display', 'about'])?> </li>
  <li><?= $this->Html->link(__('Earning calculator'), ['controller' => 'Pages', 'action' => 'display', 'earning-calculator'])?> </li>
</ul>