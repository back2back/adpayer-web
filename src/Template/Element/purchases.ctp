<?php
use Cake\I18n\Time;
?>
<div class="cell large-12">
		<h3><?= __('Purchase history')?></h3>
		<table>
			<thead>
				<tr>
					<th><?=__('ID')?></th>
					<th><?=__('Amount')?></th>
					<th><?=__('Payment source')?></th>
					<th><?=__('Status')?></th>
					<th><?=__('Purchase date')?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($allPurchases as $purchase):?>
					<tr>
						<td><?= $purchase->id?></td>
						<td><?= $purchase->amount?></td>
						<td><?= $purchase->provider ?></td>
						<td><?= $purchase->status ?></td>
						<td><?php  
						$created = new Time($purchase->created);
						echo $created->timeAgoInWords(); ?></td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>