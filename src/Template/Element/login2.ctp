<?php
?>

<div class="grid-x">
    <div class="cell large-12">
        
      <div class="login-signup-box">
        <h4><?= __('Login to your account')?></h4>
        <p><?= $this->Html->link(__('Don\'t have an account? Create a free one'), ['controller' => 'Users','action' => 'add']) ?></p>
          <?= $this->Form->create() ?>
           
                    <?php
                        
                        echo $this->Form->control('email', array('label' =>'email', 'placeholder'=>'email'));
                        
                        echo $this->Form->control('password', 
                            array('label' =>'password', 'placeholder'=>'password'));
                        ?>
                    <?= $this->Form->button(__('Login'), 
                    array('class' => 'login-sign-in button  ')) ?>
                    <?= $this->Form->end() ?>
                <p>
                   <div id="reset-password">
                            <?= $this->Html->link(__('Forgot password'), array('controller' =>'reset_tokens', 'action' => 'add'), array('class' => ''))?>
                        </div>
                </p>
      </div>  

    </div>
</div> 