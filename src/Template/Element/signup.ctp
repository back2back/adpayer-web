<?php
?>

<!DOCTYPE html>
<html class="no-js">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php
        $siteDescription = "adpayer: earn by watching videos at home";
         echo $siteDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    
    <?= $this->Html->css('foundation.min.css') ?>
    <?= $this->Html->css('app.css') ?>
    
    

    <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
     <?php 
        echo $this->Html->script('vendor/jquery', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/what-input', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/foundation', array('block' => 'scriptBottom'));
        echo $this->Html->script('app.js', array('block' => 'scriptBottom'));
    ?> 

</head>

<body>
    
<div class="title-bar" data-responsive-toggle="site-menu" data-hide-for="medium">
  <button class="menu-icon" type="button" data-toggle="site-menu"></button>
  <div class="title-bar-title">Menu</div>
</div>

<div class="top-bar" id="site-menu">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text">adPayer</li>
     
    </ul>
  </div>
  <div class="top-bar-right">
   
    <?php 
    echo $this->element('cta')
    ?>
  </div>
</div>


    <div class="grid-x">
    <div class="cell large-12">
        <div id="login">
            <div class="login-window">
                <div class="tab_menu">
                        <div class="login-tab-switch">
                            <?= $this->Html->link(__('Sign Up'), array('action' => 'add'), array('class' => 'login-selected'))?>
                            <?= $this->Html->link(__('Log In'), array('action' => 'login'), array('class' => ''))?>
                        </div>

                    <?= $this->Form->create($user) ?>
                    <?php
                        echo $this->Form->control('username', array('label' =>'', 'placeholder'=>'username'));
                        echo $this->Form->control('email', array('label' =>'', 'placeholder'=>'email'));
                        echo $this->Form->control('f_name', array('label' =>'', 'placeholder'=>'First name'));
                        echo $this->Form->control('l_name', array('label' =>'', 'placeholder'=>'Last name'));
                        echo $this->Form->control('password', array('label' =>'', 'placeholder'=>'password'));
                        ?>
                    <?= $this->Form->button('Sign Up', array('class' => 'login-sign-in button primary ')) ?>
                    <?= $this->Form->end() ?>

                    

                </div>
            </div>
        </div>

    </div>
</div>

<?php 
echo $this->fetch('scriptBottom');
?>
</body>
</html>




