<?php
?>
<ul class="list-group-horizontal">
    <li class="list-group-horizontal-item"><?= $this->Html->link(
    	__('<i class = "fi-heart "></i> ' .' Dashboard'), 
    	['controller' => 'Users', 'action' => 'dashboard'],
    	array('escape' => false));?>
    </li>
    <li class="list-group-horizontal-item"><?= $this->Html->link(
    	__('<i class ="fi-play "></i>'.' Watch videos'), 
    	['controller' => 'Playlists', 'action' => 'index'],
    	array('escape' => false)); ?></li>
    <li class="list-group-horizontal-item"><?= $this->Html->link(
    	__('<i class = "fi-link "></i>'.' Membership plans'), 
    	['controller' => 'packages', 'action' => 'index'],
    	array('escape' => false)); ?></li>
    <li class="list-group-horizontal-item"><?= $this->Html->link(
    	__('<i class = "fi-arrow-up "></i>'.' Pending purhchases'), 
    	['controller' => 'purchases', 'action' => 'index'],
    	array('escape' => false)); ?></li>
    <li class="list-group-horizontal-item"><?= $this->Html->link(
    	__('<i class = "fi-arrow-down "></i>'. 'Withdraw'), 
    	['controller' => 'payouts', 'action' => 'add'],
    	array('escape' => false)); ?></li>
    <li class="list-group-horizontal-item"><?= $this->Html->link(
        __('<i class = "fi-widget "></i>'. 'Settings'), 
        ['controller' => 'Settings', 'action' => 'overview'],
        array('escape' => false)); ?></li>
    <li class="list-group-horizontal-item"><?= $this->Html->link(
        __('<i class = "fi-torso "></i>'. 'Logout'), 
        ['controller' => 'Users', 'action' => 'logout'],
        array('escape' => false)); ?></li>
</ul>