<?php
?>
<ul class="list-group">
    <li class="list-group-item"><?= $this->Html->link(
    	__('<i class = "fi-heart fi-right"></i> ' .' Dashboard'), 
    	['controller' => 'Users', 'action' => 'dashboard'],
    	array('escape' => false));?>
    </li>
    <li class="list-group-item"><?= $this->Html->link(
    	__('<i class ="fi-play fi-right"></i>'.' Manage Users'), 
    	['controller' => 'Users', 'action' => 'index'],
    	array('escape' => false)); ?></li>
    <li class="list-group-item"><?= $this->Html->link(
    	__('<i class = "fi-link fi-right"></i>'.' Membership plans'), 
    	['controller' => 'packages', 'action' => 'index'],
    	array('escape' => false)); ?></li>
    <li class="list-group-item"><?= $this->Html->link(
        __('<i class = "fi-dollar fi-right"></i>'.' Withdrawals'), 
        ['controller' => 'windows', 'action' => 'index'],
        array('escape' => false)); ?></li>
    <li class="list-group-item"><?= $this->Html->link(
        __('<i class = "fi-pencil fi-right "></i>'.' Articles'), 
        ['controller' => 'Articles', 'action' => 'index'],
        array('escape' => false)); ?></li>
    <li class="list-group-item"><?= $this->Html->link(
        __('<i class = "fi-power fi-right "></i>'.' Logout'), 
        ['controller' => 'Users', 'action' => 'logout'],
        array('escape' => false)); ?></li>
    
</ul>