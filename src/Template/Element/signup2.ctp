<?php
?>


<div class="grid-x">
    <div class="cell large-12">
        
      <div class="login-signup-box">
        <h4><?= __('Join our community')?></h4>
        <p><?= $this->Html->link(__('Already have an account? Login'), ['controller' => 'Users','action' => 'login']) ?></p>
          <?= $this->Form->create($user, array('autocomplete' => 'false')) ?>
            <div class="grid-x">
                <div class="cell large-6">
                    <?= $this->Form->control('f_name', array('label' =>'First name', 'placeholder'=>'First name'))?>
                </div>
                <div class="cell large-6">
                    <?= $this->Form->control('l_name', array('label' =>'Last name', 'placeholder'=>'Last name'));?>
                </div>
            </div>
                    <?php
                        echo $this->Form->control('username', array('label' =>'username', 
                            'placeholder'=>'username', 'autocomplete' => 'new-username'));
                        echo $this->Form->control('email', array('label' =>'email', 'placeholder'=>'email'));
                        
                        echo $this->Form->control('password', 
                            array('label' =>'password',
                             'placeholder'=>'password',
                            'autocomplete' => 'new-password'));
                        ?>
                    <?= $this->Form->button(__('Join our community'), 
                    array('class' => 'login-sign-in button  ')) ?>
                    <?= $this->Form->end() ?>
                <p>
                    <?= __('By signing up you agree to our terms and privacy policy')?>
                </p>
      </div>  

    </div>
</div> 



