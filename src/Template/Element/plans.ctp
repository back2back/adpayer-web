<?php

?>

<div id="container">
	<?php foreach($results as $value):?>

		<div class="whole">
			
			<div  class="type">
				<p><?php echo $value['name']; ?></p>
				
				</div>
			<div class="plan">

				<div class="header">
					<?php 
					$packageCost = $value['package_cost'];
					$pieces = explode('.',$packageCost);
					?>
					<span>$</span><?php echo $pieces['0']?>
					<p class="month">per year</p>
				</div>
				<div class="content">
					<ul>
						<li><?= $value['video_load']?> <?= __('Videos per day')?> </li>
						<li><?= $value['min_payout']?> <?= __('Minimum payout')?> </li>
						<li><?= $value['expires_in_months']?> <?= __('Months valid ')?> </li>
						<li><?= $value['commission']?> <?= __('% commission earned from referral')?> </li>
					</ul>

				</div>
				<div class="bar">
					<?php 
					$packageId = $value['id'];
					
					echo $this->Html->link(__('Add to cart'), ['controller'=>'Purchases', 
						'action' => 'incomplete', $packageId],
						['class' => 'button primary button-radius']);

					 ?> 
		      		
				</div>
				
				<!-- <div class="price">
					<?php 
					$packageId = $value['id'];
					echo $this->Html->link(
						//__('Subscribe'),
						'<p class ="cart" >Add to cart</p>',
						array('controller' => 'Purchases', 'action' => 'incomplete', $packageId),
						array('escape'=>false));

					 ?> 
		      		
				</div> -->
			</div>
		</div>


	<?php endforeach;?>	

</div>
