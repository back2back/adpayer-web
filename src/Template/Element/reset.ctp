<?php
?>

<div class="grid-x">
    <div class="cell large-12">
        
        <div class="login-signup-box">  
            <h6><?= __('Please enter your email below')?></h6>
            <?= $this->Form->create($resetToken) ?>
            <?= $this->Form->control('email', array('label'=>'', 'placeholder' => __('email'))) ?>
            
            <?= $this->Form->button('Reset', array('class' => 'login-sign-in button  ')) ?>
            <?= $this->Form->end() ?>  

            <?= $this->Flash->render() ?>     
        </div>
    </div>
    </div>