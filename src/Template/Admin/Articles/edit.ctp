<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article $article
 */
?>
<div class="grid-x playlist-grid">
    <div class="cell large-12">
        <?= $this->Form->create($article) ?>
        <fieldset>
            <legend><?= __('Edit Article') ?></legend>
            <?php
                    echo $this->Form->control('topic');
                    echo $this->Form->control('body');
                    echo $this->Form->control('is_faq');
            ?>
        </fieldset>
        <?= $this->Form->button(__('Submit'), array('class' => 'button primary')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
