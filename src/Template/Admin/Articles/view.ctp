<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article $article
 */
?>
<div class="grid-x playlist-grid">
    <div class="cell large-12">
        <h3><?= h($article->id) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Topic') ?></th>
                <td><?= h($article->topic) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Written by') ?></th>
                <td><?= $article->has('user') ? $this->Html->link($article->user->f_name, ['controller' => 'Users', 'action' => 'view', $article->user->id]) : '' ?></td>
            </tr>
           
           
            <tr>
                <th scope="row"><?= __('Is Faq') ?></th>
                <td><?= $article->is_faq ? __('Yes') : __('No'); ?></td>
            </tr>
        </table>
        <div class="row">
            <h4><?= __('Body') ?></h4>
            <?= $this->Text->autoParagraph($article->body); ?>
        </div>
    </div>
</div>
