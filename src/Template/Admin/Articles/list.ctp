<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Article[]|\Cake\Collection\CollectionInterface $articles
 */
?>

<div class="grid-x">
    <div class="cell large-12">
        <?php foreach($list as $data):?>
            <ul>
                <li><?= $data?></li>
            </ul>
        <?php endforeach; ?>    
    </div>  
</div>

