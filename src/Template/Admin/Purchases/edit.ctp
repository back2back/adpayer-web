<?php
?>
<div class="grid-x playlist-grid">
	<div class="cell large-12">
		<?= $this->Form->create($purchase) ?>
    <fieldset>
        <legend><?= __('Edit Purchase') ?></legend>
        <?php

            echo $this->Form->control('amount');
            echo $this->Form->control('status', ['options' => 
            	[0 => 'unpaid',
            	1 => 'paid',
            	2 => 'failed']
        	]);
           
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), array('class' => 'button primary')) ?>
    <?= $this->Form->end() ?>
	</div>
</div>