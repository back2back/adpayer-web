<?php
?>
<div class="grid-x playlist-grid">
	<div class="cell large-12">
		<h3><?= __('Manage purchases')?></h3>
		<table>
			<thead>
				<tr>
					<th><?=__('Purchase ID')?></th>
					<th><?=__('Amount')?></th>
					<th><?=__('Package ID')?></th>
					
					<th><?=__('Payment provider')?></th>
					<th><?=__('Reference')?></th>
					<th><?=__('Status')?></th>
					<th class="actions"><?= __('Manage')?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($purchases as $purchase):?>
					<tr>
						<td><?=
 							($purchase->id);
						    					
							?>	
						</td>
						<td><?= $purchase->amount?></td>
						<td><?= $purchase->package_id ?></td>
						
						<td><?= $purchase->provider ?></td>
						<td><?= $purchase->reference?></td>
						<td><?= $purchase->status?></td>

						<td class="actions">
		                    <?= $this->Html->link(__('View'), ['action' => 'view', $purchase->id]) ?>
		                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $purchase->id]) ?>
		                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $purchase->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchase->id)]) ?>
		                </td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>
	
</div>