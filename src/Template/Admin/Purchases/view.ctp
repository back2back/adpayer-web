<?php
?>
<div class="grid-x playlist-grid">
	<div class="cell large-12">
		
		<table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($purchase->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('amount in USD') ?></th>
            <td><?= h($purchase->amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Payment provider') ?></th>
            <td><?= h($purchase->provider) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('reference') ?></th>
            <td><?= h($purchase->reference) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('status') ?></th>
            <td><?php
             $status = ($purchase->status);
             if ($status == 0) {
             	echo __('Unpaid');
             }
             if ($status == 1) {
             	echo __('Paid');
             }
             if ($status == 2) {
             	echo __('Failed');
             }

              ?>
             	
             </td>
        </tr>
        
       
        
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($purchase->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($purchase->modified) ?></td>
        </tr>
        <tr>
            
        </tr>
    </table>
	</div>
</div>