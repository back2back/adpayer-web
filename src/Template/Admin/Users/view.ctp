<?php
?>

<div class="grid-x">
	<div class="cell large-12">
		<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Profile Id') ?></th>
            <td><?= h($user->profile_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Package') ?></th>
            <td><?= $user->has('package') ? $this->Html->link($user->package->name, ['controller' => 'Packages', 'action' => 'view', $user->package->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('F Name') ?></th>
            <td><?= h($user->f_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('L Name') ?></th>
            <td><?= h($user->l_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Admin') ?></th>
            <td><?= $user->is_admin ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Playlists') ?></h4>
        <?php if (!empty($user->playlists)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Video Vendor') ?></th>
                <th scope="col"><?= __('Video Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->playlists as $playlists): ?>
            <tr>
                <td><?= h($playlists->id) ?></td>
                <td><?= h($playlists->video_vendor) ?></td>
                <td><?= h($playlists->video_id) ?></td>
                <td><?= h($playlists->user_id) ?></td>
                <td><?= h($playlists->created) ?></td>
                <td><?= h($playlists->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Playlists', 'action' => 'view', $playlists->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Playlists', 'action' => 'edit', $playlists->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Playlists', 'action' => 'delete', $playlists->id], ['confirm' => __('Are you sure you want to delete # {0}?', $playlists->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Purchases') ?></h4>
        <?php if (!empty($user->purchases)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Amount') ?></th>
                <th scope="col"><?= __('Package Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->purchases as $purchases): ?>
            <tr>
                <td><?= h($purchases->id) ?></td>
                <td><?= h($purchases->amount) ?></td>
                <td><?= h($purchases->package_id) ?></td>
                <td><?= h($purchases->user_id) ?></td>
                <td><?= h($purchases->created) ?></td>
                <td><?= h($purchases->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Purchases', 'action' => 'view', $purchases->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Purchases', 'action' => 'edit', $purchases->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Purchases', 'action' => 'delete', $purchases->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchases->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
	</div>
</div>