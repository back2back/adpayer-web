<?php
use Cake\I18n\Time;
use Cake\I18n\Date;
?>
<script src="https://d3js.org/d3.v5.min.js"></script>
<div class="grid-x playlist-grid">
	<div class="cell large-4">
		<h3><?= __('User count')?></h3>
		<p><?= $userCount?></p>
	</div>
	<div class="cell large-4">
		<h3><?= __('Packages')?></h3>
		<p><?= $packageCount?></p>
	</div>
	<div class="cell large-4">
		<h3><?= __('Subs')?></h3>
		<p><?= __('Active').' '.$activeSubs?></p>
		<p><?= __('Default') ?>
		<?php 
		$f = $subscriptionCount - $activeSubs;
		echo $f;
		?></p>
	</div>	
</div>

<div class="grid-x playlist-grid">
	<div class="cell large-12">
		<h3><?= __('Subscription distribution')?></h3>
		<div>
			<?php
			 foreach($packagesQuery as $package)
				echo $package->id. ' '. $package->name. "</br>"; 
				
			 ?>
		</div>
		<div class="sub-chart"></div>
	</div>
</div>



<div class="grid-x playlist-grid">
	<div class="cell large-12">
		<h3><?= __('Payouts')?></h3>
		

		
	</div>

	
	<div class="cell large-4">
		 <p>
			<h3><?= __('Last month')?></h3>

			<?= $lastMonthAmount ?>
		</p> 
		
		
	</div>

	<div class="cell large-4">
		<p>
			<h3><?= __('Lifetime')?></h3>
			<p>
				 <?php
				$lifetime = 0.00;
				$another = $windowsQuery;
				$lifetimeQuery = $another->find('all');
				foreach($lifetimeQuery as $data){
					$lifetime += $data->amt;
				}

				 ?>
				<?= $lifetime ?> 
			</p>

		</p>
	</div>
	<div class="cell large-4">
		<p>
			<h3><?= __('Latest')?></h3>
			<p>
				<?php
				$latest = $windowsQuery;
				$lastPayout = $latest->last();
				

				 ?>
				<?= $lastPayout->amt ?>
			</p>
		</p>
	</div>
	
</div>

<div class="grid-x playlist-grid">
	<div class="cell large-12">
		<h3><?= __('Purchases')?></h3>
	</div>
		<div class="cell large-4">
		 <p>
			<h3><?= __('Last month')?></h3>

			<?= $lastMonthIncome ?>
		</p> 
		
		
	</div>

	<div class="cell large-4">
		<p>
			<h3><?= __('Lifetime')?></h3>
			<p>
				 <?php
				$lifetime = 0.00;
				$another = $purchasesQuery;
				$lifetimeQuery = $another->find('all');
				foreach($lifetimeQuery as $data){
					$lifetime += $data->amount;
				}

				 ?>
				<?= $lifetime ?> 
			</p>

		</p>
	</div>
	<div class="cell large-4">
		<p>
			<h3><?= __('Latest')?></h3>
			<p>
				<?php
				$latest = $purchasesQuery;
				$lastPayout = $latest->last();
				

				 ?>
				<?= $lastPayout->amount ?>
			</p>
		</p>
	</div>

	
</div>

<script type="text/javascript">
	

	
	//var data = [76,90,56,32,12,120];
	var data = <?php echo json_encode(($subByGroup));?>;
	//var data = [{"a":1},{ "b":3}];

	//console.log(d3.entries(data));
	var foo = d3.entries(data);

	

	d3.select(".sub-chart")
	  .selectAll("div")
	  .data(foo, function(d){return d.key})

	    .enter()
	    .append("div")
	    .style("width", function(d) { return 15*(d.value)+ "px"; })
	    .text(function(d) { return d.key; });
</script>