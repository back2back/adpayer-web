<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Window $window
 */
?>
<div class="grid-x playlist-grid">
    

    <div class="cell large-12">
        <p><?= __('By opening the window, users will be able to make withdrawal requests. Then the admin will settle the accounts the next business day')?></p>
        <?= $this->Form->create($window) ?>
        <fieldset>
            
            
        </fieldset>
        <?= $this->Form->button(__('Open window'), array('class' => 'button primary')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
