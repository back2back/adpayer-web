<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Window $window
 */
?>
<div class="grid-x playlist-grid">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Close Window'), ['action' => 'close', $window->id]) ?> </li>
        <li><?= $this->Html->link(__('Download'), ['action' => 'export', $window->id]) ?> </li>

        
    </ul>
</nav>
<div class="windows view large-9 medium-8 columns content">
    <h3><?= h($window->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Window Key') ?></th>
            <td><?= h($window->window_key) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $window->has('user') ? $this->Html->link($window->user->id, ['controller' => 'Users', 'action' => 'view', $window->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($window->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Window Status') ?></th>
            <td><?= $this->Number->format($window->window_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amt') ?></th>
            <td><?= $this->Number->format($window->amt) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Time') ?></th>
            <td><?= h($window->start_time) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Time') ?></th>
            <td><?= h($window->end_time) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($window->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($window->modified) ?></td>
        </tr>
    </table>
</div>


</div>
<div class="grid-x playlist-grid">
    <div class="cell large-12">
        <table>
            <thead>
                <tr>
                    <th><?= __('id')?></th>
                    <th><?= __('amount')?></th>
                    <th><?= __('status')?></th>
                    <th><?= __('User btc address')?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($payouts as $payout):?>
                    <tr>
                        <td><?= $payout->id?> </td>
                        <td><?= $payout->amount?></td>
                        <td><?php
                        if ($payout->status) {
                            # payout made
                            echo __('Complete');
                        }else{
                            echo __('Incomplete');
                        }
                        ?>
                            
                        </td>
                        <td><?= $payout->btc_address ?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

