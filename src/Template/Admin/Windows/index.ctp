<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Window[]|\Cake\Collection\CollectionInterface $windows
 */
?>
<div class="grid-x playlist-grid">
<div class="cell large-12">
    <h3><?= __('Windows') ?></h3>
    <?= $this->Html->link(__('New window'), ['action' => 'add'], ['class' => 'button primary']) ?>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('window_status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('window_key') ?></th>
                
                <th scope="col"><?= $this->Paginator->sort('amt') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($windows as $window): ?>
            <tr>
                <td><?= $this->Number->format($window->id) ?></td>
                <td>
                    <?php $status = $window->window_status;
                    if ($status) {
                        echo __('open');
                    }else{
                        echo __('closed');
                    }
                     
                    ?>
                        
                </td>
                <td><?= h($window->window_key) ?></td>
                
                <td><?= $this->Number->format($window->amt) ?></td>
                <td class="actions">
                    
                    <?= $this->Html->link(__('View'), ['action' => 'view', $window->id]) ?>

                    
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
</div>
