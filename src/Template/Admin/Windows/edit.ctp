<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Window $window
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $window->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $window->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Windows'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="windows form large-9 medium-8 columns content">
    <?= $this->Form->create($window) ?>
    <fieldset>
        <legend><?= __('Edit Window') ?></legend>
        <?php
            echo $this->Form->control('window_status');
            echo $this->Form->control('window_key');
            echo $this->Form->control('start_time', ['empty' => true]);
            echo $this->Form->control('end_time', ['empty' => true]);
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('amt');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
