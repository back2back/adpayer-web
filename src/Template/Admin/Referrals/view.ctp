<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Referral $referral
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Referral'), ['action' => 'edit', $referral->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Referral'), ['action' => 'delete', $referral->id], ['confirm' => __('Are you sure you want to delete # {0}?', $referral->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Referrals'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Referral'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parent Referrals'), ['controller' => 'Referrals', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parent Referral'), ['controller' => 'Referrals', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Child Referrals'), ['controller' => 'Referrals', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Child Referral'), ['controller' => 'Referrals', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="referrals view large-9 medium-8 columns content">
    <h3><?= h($referral->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $referral->has('user') ? $this->Html->link($referral->user->id, ['controller' => 'Users', 'action' => 'view', $referral->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Parent Referral') ?></th>
            <td><?= $referral->has('parent_referral') ? $this->Html->link($referral->parent_referral->id, ['controller' => 'Referrals', 'action' => 'view', $referral->parent_referral->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($referral->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lft') ?></th>
            <td><?= $this->Number->format($referral->lft) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rght') ?></th>
            <td><?= $this->Number->format($referral->rght) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($referral->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($referral->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Referrals') ?></h4>
        <?php if (!empty($referral->child_referrals)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Lft') ?></th>
                <th scope="col"><?= __('Rght') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($referral->child_referrals as $childReferrals): ?>
            <tr>
                <td><?= h($childReferrals->id) ?></td>
                <td><?= h($childReferrals->user_id) ?></td>
                <td><?= h($childReferrals->parent_id) ?></td>
                <td><?= h($childReferrals->lft) ?></td>
                <td><?= h($childReferrals->rght) ?></td>
                <td><?= h($childReferrals->created) ?></td>
                <td><?= h($childReferrals->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Referrals', 'action' => 'view', $childReferrals->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Referrals', 'action' => 'edit', $childReferrals->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Referrals', 'action' => 'delete', $childReferrals->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childReferrals->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
