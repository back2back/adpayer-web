<?php
?>

<div class="grid-x playlist-grid">
	<div class="cell large-12">
		<div>
			<?= $this->Html->link(__('Add New'), (['action' => 'add']), (['class' => 'button primary']))?>
		</div>
	</div>

	<div class="cell large-12">
		<table>
			<thead>
				<tr>
					
					<th>name</th>
					<th>subscriber costs</th>
					<th>earning per video </th>
					<th>max videos per day</th>
					<th>validity in months</th>
					<th>Mininum payout</th>
					<th>Edit</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($packages as $package) :?>
				<tr>
				
					<td><?= $package->name?></td>
					<td><?= $package->package_cost?></td>
					<td><?=$package->cost_per_video?></td>
					<td><?= $package->video_load?></td>
					<td><?= $package->expires_in_months ?></td>
					<td><?= $package->min_payout ?></td>
					<td> <?php echo $this->Html->link(__('Edit'), ['action' => 'edit', $package->id]) ?>  </td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
</div>