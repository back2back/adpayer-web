<?php
?>

<div class="grid-x playlist-grid">
	<div class="cell large-12">
		
	    <?= $this->Form->create($package) ?>
	    <fieldset>
	        <h3><?= __('Edit Package') ?></h3>
	        <?php
	            echo $this->Form->control('name');
	            echo $this->Form->control('package_cost', ['label' => __('Cost in USD'), 'onwheel'=>"this.blur()"]);
	            echo $this->Form->control('cost_per_video', ['label' => __('Earning per video in USD'), 'onwheel'=>"this.blur()"]);
	            echo $this->Form->control('video_load', ['label' => __('Videos per day'), 'onwheel'=>"this.blur()"]);
	            echo $this->Form->control('expires_in_months', ['label' => __('Valid (months)'), 'onwheel'=>"this.blur()"]);
	            echo $this->Form->control('min_payout', ['label' => __('Minimum amount for withdrawal USD'), 'onwheel'=>"this.blur()"]);
	            echo $this->Form->control('commission', ['label' => __('Perentage commission from referrals'), 'onwheel'=>"this.blur()"]);


	        ?>
	    </fieldset>
	    <?= $this->Form->button(__('Update'), array('class' => 'button primary')) ?>
	    <?= $this->Form->end() ?>
	
	</div>
</div>