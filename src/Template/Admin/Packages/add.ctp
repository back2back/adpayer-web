<?php
?>

<div class="grid-x playlist-grid">
	<div class="cell large-12">

		<p>
			<?= __('Should be setting the values such that even in 30 days(1 month) the earnings from video should not be greater than the cost of the subscriptions')?>
		</p>
		
		<?= $this->Form->create($package) ?>
	    <fieldset>
	        <legend><?= __('Add Package') ?></legend>
	        <?php
	            echo $this->Form->control('name');
	            echo $this->Form->control('package_cost', ['label' => __('Cost in USD'), 'onwheel'=>"this.blur()"]);
	            echo $this->Form->control('cost_per_video', ['label' => __('Earning per video in USD'), 'onwheel'=>"this.blur()"]);
	            echo $this->Form->control('video_load', ['label' => __('Videos per day'), 'onwheel'=>"this.blur()"]);
	            echo $this->Form->control('expires_in_months', ['label' => __('Valid (months)'), 'onwheel'=>"this.blur()"]);
	            echo $this->Form->control('min_payout', ['label' => __('Minimum amount for withdrawal USD'), 'onwheel'=>"this.blur()"]);
	            echo $this->Form->control('commission', ['label' => __('Perentage commission from referrals'), 'onwheel'=>"this.blur()"]);

	        ?>
	    </fieldset>
	    <?= $this->Form->button(__('Submit'), array('class' => 'button primary')) ?>
	    <?= $this->Form->end() ?>


	</div>
</div>

