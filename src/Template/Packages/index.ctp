<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Package[]|\Cake\Collection\CollectionInterface $packages
 */
?>

<div class="grid-x playlist-grid">
    <div class="large-12 cell">
        <div class="callout success">
            <h5><?= __('You are currently on the following plan')?></h5>
            <p><?php 

            foreach($subscriptionsQuery as $data){
               
                echo $data->package->name;
            }

             ?></p>
        </div>
        <div id="packages">
            <?php 
            $results = $results->toArray();
             echo $this->element('plans', $results)?>
        </div>
    </div>
</div>


