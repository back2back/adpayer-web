<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Coinpayment $coinpayment
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Coinpayment'), ['action' => 'edit', $coinpayment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Coinpayment'), ['action' => 'delete', $coinpayment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $coinpayment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Coinpayments'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Coinpayment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="coinpayments view large-9 medium-8 columns content">
    <h3><?= h($coinpayment->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Transaction') ?></th>
            <td><?= h($coinpayment->transaction) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Checkout Url') ?></th>
            <td><?= h($coinpayment->checkout_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status Url') ?></th>
            <td><?= h($coinpayment->status_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Qrcode Url') ?></th>
            <td><?= h($coinpayment->qrcode_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $coinpayment->has('user') ? $this->Html->link($coinpayment->user->id, ['controller' => 'Users', 'action' => 'view', $coinpayment->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($coinpayment->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->format($coinpayment->amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Confirms Needed') ?></th>
            <td><?= $this->Number->format($coinpayment->confirms_needed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Timeout') ?></th>
            <td><?= $this->Number->format($coinpayment->timeout) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($coinpayment->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($coinpayment->modified) ?></td>
        </tr>
    </table>
</div>
