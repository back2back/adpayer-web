<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Coinpayment $coinpayment
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Coinpayments'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="coinpayments form large-9 medium-8 columns content">
    <?= $this->Form->create($coinpayment) ?>
    <fieldset>
        <legend><?= __('Add Coinpayment') ?></legend>
        <?php
            echo $this->Form->control('amount');
            echo $this->Form->control('transaction');
            echo $this->Form->control('confirms_needed');
            echo $this->Form->control('timeout');
            echo $this->Form->control('checkout_url');
            echo $this->Form->control('status_url');
            echo $this->Form->control('qrcode_url');
            echo $this->Form->control('user_id', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
