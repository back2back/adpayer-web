<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Coinpayment[]|\Cake\Collection\CollectionInterface $coinpayments
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Coinpayment'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="coinpayments index large-9 medium-8 columns content">
    <h3><?= __('Coinpayments') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('transaction') ?></th>
                <th scope="col"><?= $this->Paginator->sort('confirms_needed') ?></th>
                <th scope="col"><?= $this->Paginator->sort('timeout') ?></th>
                <th scope="col"><?= $this->Paginator->sort('checkout_url') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status_url') ?></th>
                <th scope="col"><?= $this->Paginator->sort('qrcode_url') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($coinpayments as $coinpayment): ?>
            <tr>
                <td><?= $this->Number->format($coinpayment->id) ?></td>
                <td><?= $this->Number->format($coinpayment->amount) ?></td>
                <td><?= h($coinpayment->transaction) ?></td>
                <td><?= $this->Number->format($coinpayment->confirms_needed) ?></td>
                <td><?= $this->Number->format($coinpayment->timeout) ?></td>
                <td><?= h($coinpayment->checkout_url) ?></td>
                <td><?= h($coinpayment->status_url) ?></td>
                <td><?= h($coinpayment->qrcode_url) ?></td>
                <td><?= h($coinpayment->created) ?></td>
                <td><?= h($coinpayment->modified) ?></td>
                <td><?= $coinpayment->has('user') ? $this->Html->link($coinpayment->user->id, ['controller' => 'Users', 'action' => 'view', $coinpayment->user->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $coinpayment->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $coinpayment->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $coinpayment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $coinpayment->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
