<?php
?>

<div class="grid-x">
	<div class="cell large-12">
		<h3><?= __('Choose how you want to pay ')?></h3>
		<p>
			
			<?php foreach ($wallets as $key) {
				
				if (array_key_exists('PerfectMoney', $key)) {
					//echo $this->Html->link(__('PerfectMoney'), ['action' => 'pay_method', $packageId,'perfect']);
					echo $this->Html->link(
						$this->Html->image('banner4.png'),
						['action' => 'pay_method', $packageId,'perfect'],
						['escape' => false]
					);
					echo '<br>';
				}
				if (array_key_exists('Payeer', $key)) {
					//echo $this->Html->link(__('Payeer'), ['action' => 'pay_method', $packageId,'payeer']);
					echo $this->Html->link(
						$this->Html->image('banner5.png'),
						['action' => 'pay_method', $packageId,'payeer'],
						['escape' => false]
					);
				}
			} ?> 
			
		</p>
	</div>
</div>