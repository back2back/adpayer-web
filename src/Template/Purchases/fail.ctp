<?php
use Cake\Core\Configure;
?>

<div class="grid-x">
	<div class="cell large-12">
		<h3><?= __('Sorry, your purchase failed')?></h3>
		<p> <?= __('Please contact admin at ') ?> </p>
		<p> <?= Configure::read('adminEmail') ?></p>
	</div>
</div>