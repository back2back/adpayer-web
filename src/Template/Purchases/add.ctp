<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Purchase $purchase
 */
?>

<div class="grid-x">
    <div class="cell large-12 medium-12 small-12">
        <h3><?= __('Choose how you want to pay ')?></h3>
        
        <p><?php  echo $package->name;?></p>
        <p><?= $package->package_cost?></p>
        <?= $this->Form->create($purchase) ?>
           
                
                <?php
                    echo $this->Form->control('amount');
                   
                ?>
           
            <?= $this->Form->button(__('Submit'), array('class' => 'button primary')) ?>
            <?= $this->Form->end() 
        ?>
        </div>
</div>
