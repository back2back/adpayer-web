<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Purchase[]|\Cake\Collection\CollectionInterface $purchases
 */
?>
<div class="grid-x playlist-grid">
    <div class=" large-9 medium-8 cell">
        <h3><?= __('PendingPurchases') ?></h3>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th scope="col"><?= __('id')?></th>
                    <th scope="col"><?= __('amount') ?></th>
                    <th scope="col"><?= __('checkout')?></th>
                    
                </tr>
            </thead>
            <tbody>
                <?php foreach ($purchases as $purchase): ?>
                <tr>
                    <td><?= h($purchase->id) ?></td>
                    <td><?= $this->Number->format($purchase->amount) ?></td>
                   
                    
                    
                    <td class="actions">
                        <?= $this->Html->link(__('Checkout now'), [
                            'class' => 'primary button',
                            'action' => 'completeCheckout', $purchase->id, $purchase->reference]) ?>
                        
                        
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        
    </div>
</div>
