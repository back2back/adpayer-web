<?php
?>
<div class="grid-x playlist-grid">
	<div class="cell large-12">
		<h3><?= __('You have a pending transaction')?></h3>
		<?= $this->Form->create() ?>
		<?= $this->Form->radio('proceed', ['1' =>'Yes', '0'=>'No'], ['required' => true]) ?>
		<?= $this->Form->button(__('Proceed to checkout'), ['type' => 'submit','class' => 'primary button']) ?>
		<?= $this->Form->end() ?>
	</div>
</div>