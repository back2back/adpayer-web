<?php
?>

<div class="grid-x">
	<div class="cell large-12">
		<?php echo $this->element('invoice') ?>


	<?php echo $this->Form->create(null, array('url' => $url))?>
	<?= $this->Form->hidden('PAYEE_ACCOUNT', array('value' => $payeeAccount))?>
	<?= $this->Form->hidden('PAYEE_NAME', array('value' => $payeeName))?>
	<?= $this->Form->hidden('PAYMENT_ID', array(
		'value' => 'subscription '. $packageEntity->name.' '. $packageEntity->package_cost.' '.'for '. $loggedInUser['email']))?>
	<?= $this->Form->hidden('PAYMENT_AMOUNT', array('value' => $packageEntity->package_cost)) ?>
	<?= $this->Form->hidden('PAYMENT_UNITS', array('value' => 'USD')) ?>
	<?= $this->Form->hidden('STATUS_URL', array('value' => '')) ?>
	<?= $this->Form->hidden('PAYMENT_URL', array('value' => $baseUrl.'purchases/success')) ?>
	<?= $this->Form->hidden('PAYMENT_URL_METHOD', array('value' => 'GET')) ?>
	<?= $this->Form->hidden('NOPAYMENT_URL', array('value' => $baseUrl.'purchases/fail')) ?>
	<?= $this->Form->hidden('NOPAYMENT_URL_METHOD', array('value' => 'GET')) ?>
	<?= $this->Form->hidden('SUGGESTED_MEMO', array('value' => __('Looking forward to serve you'))) ?>
	
	
	<?= $this->Form->button(__('Proceed to pay'), array('class' => 'button primary pay')) ?>
    <?= $this->Form->end();?> 
        


	

		
	</div>
</div>