<?php
?>

<div class="grid-x">
	<div class="cell large-12">
		<div id="faq-header">
			<h1 class="center-absolute" ><?= __('FAQ')?></h1>
		</div>
		
	</div>
</div>

<div class="grid-x">
	<div class="cell large-12">
		<h3><?= $article->topic?></h3>
		<p><?= $article->body?></p>
		<p><?= $this->Html->link(__('Back'), ['action' => 'index']) ?></p>
	</div>
</div>