<?php
?>
	
<div class="grid-x">
	<div class="cell large-12">
		<div id="faq-header">
			<h1 class="center-absolute" ><?= __('FAQ')?></h1>
		</div>
		
	</div>
</div>
<div class="grid-x">
	<div class="large-12 cell">
		<?php foreach($articles as $article):?>
			<h3><?= $this->Html->link(__($article->topic), ['action' => 'view', $article->id])?></h3>
		<?php endforeach;?>
	</div>
</div>

<div class="grid-x">
	<div class="cell large-12">
		<?= $this->Html->link(__('Home'), ['action' => '/','controller'=>'pages' ])?>
	</div>
</div>

