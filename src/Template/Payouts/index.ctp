<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Payout[]|\Cake\Collection\CollectionInterface $payouts
 */
?>
<div class="grid-x playlist-grid">
    

<div class="cell large-12">
    <h3><?= __('Payouts') ?></h3>
    <table>
        <thead>
            <tr>
                <th scope="col"><?=('id') ?></th>
                <th scope="col"><?= ('amount') ?></th>
                <th scope="col"><?= ('created') ?></th>
                <th scope="col"> <?= __('status')?></th>
                
            </tr>
        </thead>
        <tbody>
            <?php foreach ($payouts as $payout): ?>
            <tr>
                <td><?= h($payout->id) ?></td>
                <td><?= $this->Number->format($payout->amount) ?></td>
                <td><?= h($payout->created) ?></td>
                <td><?php 
                if ($payout->status) {
                    echo __('Complete');
                }else{
                    echo __('Pending');
                }
                ?></td>
                
                
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
</div>

</div>
