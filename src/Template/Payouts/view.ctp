<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Payout $payout
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Payout'), ['action' => 'edit', $payout->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Payout'), ['action' => 'delete', $payout->id], ['confirm' => __('Are you sure you want to delete # {0}?', $payout->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Payouts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Payout'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="payouts view large-9 medium-8 columns content">
    <h3><?= h($payout->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($payout->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->format($payout->amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($payout->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($payout->modified) ?></td>
        </tr>
    </table>
</div>
