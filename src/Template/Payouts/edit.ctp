<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Payout $payout
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $payout->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $payout->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Payouts'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="payouts form large-9 medium-8 columns content">
    <?= $this->Form->create($payout) ?>
    <fieldset>
        <legend><?= __('Edit Payout') ?></legend>
        <?php
            echo $this->Form->control('amount');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
