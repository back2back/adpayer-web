<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Payout $payout
 */
?>

<div class="grid-x playlist-grid">
     <div class="large-12 cell">

        <div class="grid-x">
            <div class="large-12 cell">
                <p>
                    <?= __('Your balance is ')?>
                </p>
                <h5><?= __('USD')?></h5>
                <h3><?php 
                $cumulativeRef = 0;
                foreach($ref_earnings as $refData)
                        {
                            $myReferrals = $refData->amount;
                            $cumulativeRef += $myReferrals;
                        } 
                    ?>
                    <?= $cumulativeRef + $main_balance; ?>
                    
                </h3>
            </div>
            

            <div class="large-12 cell">
                <h5><?= __('Payout breakdown')?></h5>
                <ul>
                    <li><?= __('From my activity').' '. $main_balance; ?></li>
                    <?php foreach($ref_earnings as $refData):?>
                        <li><?= __('From my referrals').' '. $refData->amount; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <?php if($noBTCAddress):?>
            <div class="grid-x">
                <div class="cell large-12">
                    <p><?= __('You have no bitcoin address info. Input it below ')?></p>
                    <p><?= $this->Html->link(__('Link your bitcoin (BTC) address'), ['controller' => 'Settings', 'action' => 'overview'],
                        ['class' => 'button primary']) ?></p>
                </div>
            </div>
        <?php else: ?>

        <div class="payouts form large-9 medium-8 columns content">
            <?= $this->Form->create($payout) ?>
            <fieldset>
                <legend><?= __('Add Payout') ?></legend>
                <?php
                    //echo $this->Form->hidden('amount');
                    //echo $this->Form->control('user_id', array('type' => 'hidden'));
                ?>
            </fieldset>
            <?= $this->Form->button(__('Withdraw'), array('class' => 'button primary')) ?>
            <?= $this->Form->end() ?>
        </div>

    <?php endif;?>
    <div class="cell large-12">
        <p><?= $this->Html->link(__('Previous Withdrawals'), [ 'action' => 'index']) ?>
                            
        </p>
    </div>
</div>
</div>

