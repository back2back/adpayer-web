<?php
use Cake\I18n\Time;

$global = new Time();
?>

<div class="grid-x playlist-grid">
	<div class="cell large-6">
		<h3>
			<?= __('Account Summary')?>
			
		</h3>

		<div class="grid-x">
			<div class="cell large-4">
				<p><?= $this->Html->image('man-smile-testimonial.png')?></p>
			</div>
			<div class="cell large-8">
				<!-- Name and profiile-->

				<p>
					<ul class="user-stats">
						<li>
							<h5><?= __('Username')?></h5>
							<h6><?= $user['username']?></h6>
						</li>
						<li>
							<h5><?= __('Name')?></h5>
							<h6><?= $user['f_name'].' '.$user['l_name'] ?></h6>
						</li>
						<li>
							<h5><?= __('Email')?></h5>
							<h6><?= $user['email']?></h6>
						</li>
						<li>
							<h5><?= __('Joined')?></h5>
							<h6><?php $joined = new Time($user['created']);
								echo $joined->timeAgoInWords();
								?>
								
							</h6>
						</li>
						<li>
							<h5><?=__('Plan')?></h5>
								<h6>
									<?php
									foreach($subscriptionQuery as $data){
										echo $data->package->name;
									}
									?>
								</h6>
							
						</li>
					</ul>
				</p>

			</div>
		</div>

	</div>
	<div class="cell large-6">
		<!-- Account data-->
		<h3><?= __('Account balance')?></h3>
		<p> <?= $totalBal?> </p>
		<p>
			<?= $this->Html->link(__('Withdraw'),
			['controller'=>'Payouts','action' => 'add'],
			['class' => 'primary button'])?>
		</p>
		<p>
			<h3><?= __('Referral link')?></h3>
			<?php if (isset($referralUrl)) :?>
				<input type="text" value="<?=$referralUrl?>" id="myInput">
				<button id="myInput"  class="primary button" onclick="myFunction()">Copy my referral link</button>
				<?php else :?>
					<?= $this->Html->link(__('Create my referral link'),
					['controller'=> 'Referrals', 
					 'action' => 'add', $user['id'],0])?>
			<?php endif?>
		</p>
	</div>

	
	<!-- table structure -->
	<div class="cell large-12">
		<h3><?= __('Playlist')?></h3>
		<table>
			<thead>
				<tr>
					<th><?=__('Date')?></th>
					<th><?=__('Source')?></th>
					<th><?=__('Video ID')?></th>
					<th><?=__('Earn')?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($myLastVideosQuery as $playlist):?>
					<tr>
						<td><?=
 							($playlist->created);
						    					
							?>	
						</td>
						<td><?= $playlist->video_vendor?></td>
						<td><?= $playlist->video_ref ?></td>
						<td><?= $playlist->earn ?></td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>

	<?= $this->element('purchases')?>

	

	<div class="cell large-12">
		<h3><?= __('Referrals')?></h3>
		<table>
			<thead>
				<tr>
					<th><?=__('Username')?></th>
					<th><?=__('Email')?></th>
					<th><?=__('Joined')?></th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach($downLine as $data):?>
					<tr>
						<td><?= $data->user->username?></td>
						<td><?= $data->user->email?></td>
						<td><?php 
						$created = new Time($data->created);
						echo $created->timeAgoInWords(); 
						?></td>
					
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>

	

	<!-- end table structure-->
</div>


<!-- <div id="dashboard-container">
	<div class="grid-x">
		<div id="dashboard-header" class="large-12 cell cardify stats-header" >
			<div class="grid-x">
				<div class="cell small-12 large-4">
					<p><?= $this->Html->image('man-smile-testimonial.png')?></p>
					<p>
						<?= __('Balance')?>
						<?= ' '?>
						<?= $profileData->amount?>
							
					</p>

					<p>
						<?php if (isset($referralUrl)) :?>
						<input type="text" value="<?=$referralUrl?>" id="myInput">
						<button id="myInput"  class="primary button" onclick="myFunction()">Copy my referral link</button>
						<?php else :?>
							<?= $this->Html->link(__('Create my referral link'),
							['controller'=> 'Referrals', 
							 'action' => 'add', $user['id'],0])?>
						<?php endif?>	
					</p>
				</div>
				<div class="cell large-8">
					<div class="">
						<p>
							<ul class="user-stats">
								<li>
									<h5><?= __('Username')?></h5>
									<h6><?= $user['username']?></h6>
								</li>
								<li>
									<h5><?= __('Name')?></h5>
									<h6><?= $user['f_name'].' '.$user['l_name'] ?></h6>
								</li>
								<li>
									<h5><?= __('Email')?></h5>
									<h6><?= $user['email']?></h6>
								</li>
								<li>
									<h5><?= __('Joined')?></h5>
									<h6><?php $joined = new Time($user['created']);
										echo $joined->timeAgoInWords();
										?>
										
									</h6>
								</li>
							</ul>
						</p>
					</div>
				</div>
			</div>

			

		</div>
	</div>

	<div class="grid-x">
		<div class="cell large-6 medium-6 small-12">
			<div class="cardify">
				<div class="snapshot"><h5><?= __('Watched videos')?></h5></div>
				<p class="stats"><?= $count?></p>
				<p class="stats-details"><?= __('Since last payout')?></p>
			</div>
		</div>

		<div class="cell large-6 medium-6 ">
			<div class="cardify">
				<div class="snapshot"><h5><?= __('Earnings')?></h5></div>
				<p class="stats"><?= $earnings?></p>
				<p class="stats-details"><?= __('Since last payout')?></p>
			</div>
		</div>
	</div>

	<div class="grid-x">
		<div class="cell large-4 medium-4 small-12">
			<div class="cardify">
				<div class="snapshot"><h5><?= __('Membership plan')?></h5></div>
				<p class="stats">
					<?php
					foreach($subscriptionQuery as $data){
						echo $data->package->name;
					}
					?>
					
				</p>
			</div>
		</div>
		<div class="cell large-4 medium-4 ">
			<div class="cardify">
				<div class="snapshot"><h5><?= __('Last purchase')?></h5></div>
				<p class="stats"><?= $purchaseLast->amount?></p>
			</div>
		</div>
		<div class="cell large-4 medium-4 ">
			<div class="cardify">
				<div class="snapshot"><h5><?= __('Withdrawal')?></h5></div>
				<p class="stats"><?= $payoutLast->amount?></p>
			</div>
		</div>
	</div>
</div> -->
<script type="text/javascript">
	function myFunction() {
	  /* Get the text field */
	  var copyText = document.getElementById("myInput");

	  /* Select the text field */
	  copyText.select();
	  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

	  /* Copy the text inside the text field */
	  document.execCommand("copy");

	  /* Alert the copied text */
	  alert("Copied the text: " + copyText.value);
	}
</script>