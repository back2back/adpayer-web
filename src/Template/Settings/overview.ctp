<?php
?>


<div class="grid-x playlist-grid">
	<div class="large-12 cell">
		<h4><?= __('Payout info')?></h4>
		<?= $this->Form->create($profileData); ?>
		<?= $this->Form->control('btc_address') ?>

		<?= $this->Form->button(__('Update'), ['class' => 'button primary']) ?>
    	<?= $this->Form->end() ?>

	</div>
</div>
