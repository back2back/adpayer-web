<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ResetToken $resetToken
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $resetToken->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $resetToken->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Reset Tokens'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="resetTokens form large-9 medium-8 columns content">
    <?= $this->Form->create($resetToken) ?>
    <fieldset>
        <legend><?= __('Edit Reset Token') ?></legend>
        <?php
            echo $this->Form->control('token');
            echo $this->Form->control('email');
            echo $this->Form->control('is_valid');
            echo $this->Form->control('expires', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
