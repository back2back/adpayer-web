<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ResetToken[]|\Cake\Collection\CollectionInterface $resetTokens
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Reset Token'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="resetTokens index large-9 medium-8 columns content">
    <h3><?= __('Reset Tokens') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('token') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_valid') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('expires') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($resetTokens as $resetToken): ?>
            <tr>
                <td><?= $this->Number->format($resetToken->id) ?></td>
                <td><?= h($resetToken->token) ?></td>
                <td><?= h($resetToken->email) ?></td>
                <td><?= $this->Number->format($resetToken->is_valid) ?></td>
                <td><?= h($resetToken->created) ?></td>
                <td><?= h($resetToken->modified) ?></td>
                <td><?= h($resetToken->expires) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $resetToken->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $resetToken->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $resetToken->id], ['confirm' => __('Are you sure you want to delete # {0}?', $resetToken->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
