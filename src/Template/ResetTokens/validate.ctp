<?php 

$this->layout = 'no_layout';
?>

<section class="intro">
<?php 

    echo $this->element('top_bar_with_logo');
?>

<div class="grid-x ">
    <div class="center-absolute">
        <div class="cell large-12">
           
            <div class="login-signup-box">                 
                        <h6><?= __('Update password')?></h6>

                        <?= $this->Form->create() ?>
                        <?= $this->Form->control('password', array('label'=>'', 'placeholder' => __('password'))) ?>
                        
                        
                        <?= $this->Form->button('Update password', array('class' => 'login-sign-in button primary ')) ?>
                        <?= $this->Form->end() ?>

            </div>      

    </div>       

        </div>
</div>

</section>











