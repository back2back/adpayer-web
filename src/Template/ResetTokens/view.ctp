<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ResetToken $resetToken
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Reset Token'), ['action' => 'edit', $resetToken->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Reset Token'), ['action' => 'delete', $resetToken->id], ['confirm' => __('Are you sure you want to delete # {0}?', $resetToken->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Reset Tokens'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Reset Token'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="resetTokens view large-9 medium-8 columns content">
    <h3><?= h($resetToken->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Token') ?></th>
            <td><?= h($resetToken->token) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($resetToken->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($resetToken->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Valid') ?></th>
            <td><?= $this->Number->format($resetToken->is_valid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($resetToken->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($resetToken->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expires') ?></th>
            <td><?= h($resetToken->expires) ?></td>
        </tr>
    </table>
</div>
