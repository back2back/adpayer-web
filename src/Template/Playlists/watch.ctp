<?php
use Cake\Core\Configure;
use Cake\Utility\Text;
$debug = Configure::read('debug');
?>
<?php if($debug): ?>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<?php else:  ?>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>
<?php endif;?>
<script src="https://api.dmcdn.net/all.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>


	<div class="grid-x playlist-grid">
    <div id ="video-content-flash" class="large-12 cell" v-if="seen">
      <?php if($legit): ?>

      <div class="callout success">
        <p><?= __('Video added to daily watch list')?></p>
      </div>

      <?php else:  ?>
      <div class="callout alert">
        <p><?= __('Daily watch limit reached.')?><?= $this->Html->link(__('Unlock account by upgrading'), 
          ['controller' => 'packages', 'action' => 'index']
          )?>
          
        </p>
      </div>
    <?php endif;?>
    </div>
		<div class="cell large-8">
			

			<div class="grid-y">
				<div class="cell large-5 medium-5 small-6">	
					<div id="player">
				
					</div>
				</div>
				<div class="cell large-7 medium-7 small-6 ">
					<h3 ><?php echo $response['title']?></h3>
					<p class="hide-for-small-only "><?php echo  $response['description']?></p>
				</div>
			</div>
			
		</div>
		<div id="related" class="cell large-4 ">
			<h3><?= __('Related')?></h3>
			<?php foreach($related['list'] as $value):?>
				
				<div class="card">
                        <?php 
                        echo $this->Html->link(
                            $this->Html->image($value['thumbnail_120_url'], array('class' => 'related-thumb')),
                            "/playlists/watch/".$value['id'],
                            ['escape' => false]
                            );
                        ?>
                        
                        <?php 

                     
                        ?>
                        <div class="card-section">
                            <p>
                            <?php 
                            $truncated = Text::truncate($value['title'], 35,['ellipsis' => '...','exact' => false]);
                            echo $this->Html->link(
                                $truncated,
                                 "/playlists/watch/".$value['id'],
                                 array('class' => 'video-title')
                                 
                                        );?>
                            </p>
                            
                            <div class="vid-duration">
                                <i class="fi-clock"></i>
                                <?= gmdate("H:i:s", $value['duration']); ?>
                                <br>
                                <i class="fi-torso"></i>
                                <?= $value['owner.screenname']?>
                            </div>
                        </div>
                        
                    </div>


			
		<?php endforeach; ?>
		</div>
	</div>



<script>

var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;


var app = new Vue({
  el: '#video-content-flash',
  data : {
    seen: false,
    video: ''
  },
  methods: {
    addToWatched(){

      /*const instance = axios.create({
      baseURL: "<?php echo $playlistUrl ?>",
      timeout: 1000,
      headers: {'X-CSRF-Token': csrfToken}
    });*/
       
    /*axios.post("<?php echo $playlistUrl ?>", {
      video_ref: "<?php echo $dailymotionVideo?>",
      user_id: 'Flintstone',

      
  })*/

  axios({
  method: 'post',
  url: "<?php echo $playlistUrl ?>",
  data: {
    video_ref: "<?php echo $dailymotionVideo?>"
    
  },
  headers: {
    'X-CSRF-Token': 'd71a69d71c11215d31c1bebd2e5365878cf894a592fe7d1ee6cc31d540bc23e5407d05ffadb6a6288066e06a28e750c417a61cd88a978af186b43d0ab345e25d'
  },
  
})

  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
    
  });


  }
}});

var player = DM.player(document.getElementById("player"), {
    video: "<?php echo $dailymotionVideo?>",
    width: "100%",
    height: "480",
    params: {
        autoplay: true,
        mute: false,
        'queue-autoplay-next': false,
        'queue-enable':false
    }


});

player.addEventListener('start', function(event) {
  console.log('event '+event.type+' received on '+event.target+'!');
  
  setTimeout(function(){
  	app.seen = true;
    app.addToWatched();
  }, "<?php echo $timeout?>");
  //app.message = "yyy";

});



</script>
