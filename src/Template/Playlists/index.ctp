<?php

use Cake\Utility\Text;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Playlist[]|\Cake\Collection\CollectionInterface $playlists
 */

?>

   

<div class="grid-x playlist-grid">
    
       
    <?php foreach($response['list'] as $value):?>
        <?php 

         ?>
        <div class="large-4 medium-6 small-12 cell auto">
            <div class="card">
                <?php 
                echo $this->Html->link(
                    $this->Html->image($value['thumbnail_240_url'], array('class' => 'vid-thumb')),
                    "/playlists/watch/".$value['id'],
                    ['escape' => false]
                    );
                ?>
                
                <?php 

                //echo $this->Html->image($value['thumbnail_240_url'], array('class' => 'vid-thumb'));
                
                ?>
                <div class="card-section">
                    <p>
                    <?php 
                    $truncated = Text::truncate($value['title'], 35,['ellipsis' => '...','exact' => false]);
                    echo $this->Html->link(
                        $truncated,
                         "/playlists/watch/".$value['id'],
                         array('class' => 'video-title')
                         
                                );?>
                    </p>
                    
                    <div class="vid-duration">
                        <i class="fi-clock"></i>
                        <?= gmdate("H:i:s", $value['duration']); ?>
                        <br>
                        <i class="fi-torso"></i>
                        <?= $value['owner.screenname']?>
                    </div>
                </div>
                
            </div>  
        </div>
    <?php endforeach;?>
           
</div>
