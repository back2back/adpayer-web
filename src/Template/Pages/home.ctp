<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = false;



$siteDescription = 'Earn money online by watching videos';
?>



<!DOCTYPE html>
<html class="no-js">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name = "payeer-shop-id" content = "865294319">
    <title>
        <?= $siteDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    
    <?= $this->Html->css('foundation.min.css') ?>
    <?= $this->Html->css('app.css') ?>
    <?= $this->Html->css('foundation-icons.css') ?>
    
    

    <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
     <?php 
        echo $this->Html->script('vendor/jquery', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/what-input', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/foundation', array('block' => 'scriptBottom'));
        echo $this->Html->script('app.js', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/jquery.waypoints.min', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/jquery.counterup.min', array('block' => 'scriptBottom'));
    ?> 

</head>
<body class="home">


 <div class="title-bar" data-responsive-toggle="site-menu" data-hide-for="medium">
  <button class="menu-icon" type="button" data-toggle="site-menu"></button>
  <div class="title-bar-title">Menu</div>
</div>

<div class="top-bar" id="site-menu">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text">adPayer</li>
     
    </ul>
  </div>
  <div class="top-bar-right">
   
     <?php 
       
        
        if (isset($loggedInUser['id'])) {
          # code...

          echo 'Hi '. $loggedInUser['f_name'];
        }else{
           echo $this->element('cta');
        }
       
        ?>
  </div>
</div>
 

<div  class="grid-x">
    <div class="cell large-12">
        <div class="hero">
            <video  autoplay muted loop poster="./img/cloud.jpg" id="bgvideo" class="hero-video">
                <source src="./img/adpayer-hero.mp4" type="video/mp4">
            </video> 
            <div class="box-text"> 
                    <h1>Earn money</h1>
                    <h4>Anywhere.Anytime.Watching videos</h4>
                    <?= $this->element('cta')?>
            </div>
        </div>
    </div>
    
</div>

<div id="how-it-works" >
    <div class="grid-x">
        <div class="cell large-12">
            <h3 class="header-center-margin"><?= __('Services')?></h3>
            <span class="separator"></span>
        </div>
    </div>

    <div class="grid-x home-content text-center text-center">
        <div class="cell large-4 medium-4">
            <?= $this->Html->image("001-like.png", array('class' => 'icon-outline'))?>
            <h4 class="service-heading"><?=__('Loved by advertisers')?></h4>
            <p><?= __('Our user base keeps growing month over month.')?></p>

        </div>
        <div class="cell large-4 medium-4">
            <?= $this->Html->image("002-computer.png", array('class' => 'icon-outline'))?>
            <h4 class="service-heading"><?= __('Web and mobile')?></h4>
             <p><?= __('Responsive design works on mobiles, tablets and PC')?></p>
               
            
        </div>
        <div class="cell large-4 medium-4">
            <?= $this->Html->image("locked.png", array('class' => 'icon-outline'))?>
            <h4 class="service-heading"><?= __('Secure')?></h4>
            <p><?= __('Our payments are processed securely. Your privacy is our priority')?></p>
        </div> 
    </div>


    
</div>

<div id="description">
    <div class="grid-x">
        <div class="cell large-12">
            <h3 class="header-center-margin"><?= __('Boost your income')?></h3>
            <span class="separator"></span>
        </div>
    </div>
    <div class="grid-x home-content text-center">
        <div class="large-6 cell">
            <?= $this->Html->image("man-video.jpg", array('class'=>'image-description-left'))?>
        </div>
        <div class="large-6 cell">
            <h2><?= __('Watch videos')?></h2>
            <p><?= __('Works seemlessly from mobile, tablets and PC.')?></p>
        </div>
    </div>
    
    <div class="grid-x home-content text-center">
        
        <div class="large-6 cell">
            <h2><?= __('Grow your network')?></h2>
            <p><?= __('Use your referral link to invite others and earn special rewards.')?></p>
        </div>
        <div id="leon" class="large-6 cell">
            <div id="image-wrapper">
                <?= $this->Html->image("https://d1fto35gcfffzn.cloudfront.net/images/home/index/2019/network.svg", array('id' => 'wire'))?>
                <a href="#" class="cloud-circle" id="google">
                    <img src="./img/man.png">
                </a>
                <a href="#" class="cloud-circle" id="aws">
                    <img src="./img/girl.png" alt="">
                </a>
                <a href="#" class="cloud-circle" id="microsoft">
                   
                   <img data-interchange="[./img/boy.png, medium], [./img/boy-small.png, small]">
                </a>
            </div>
           
        </div>
    </div>

    

    <div class="grid-x home-content text-center">
        <div class="large-6 cell">
            <?= $this->Html->image('withdraw.jpg')?>
        </div>
        <div class="large-6 cell">
            <h2><?= __('Withdraw')?></h2>
            <p><?= __('Withdraw your earnings every week to your preferred e-wallet')?></p>
        </div>
    </div>
    

</div>

<div id="stats">
    <div class="grid-x home-content text-center">
        <div class="cell large-3">    
            <h2>
                <div class="icon-box"><i class="fi-torsos"></i></div>
                <span class="counter">721</span> 
                
            </h2>
            <p>
                <?= __('Members')?>
            </p>
        </div>
        <div class="cell large-3">    
            <h2>
                <div class="icon-box"><i class="fi-graph-bar"></i></div>
                <span class="counter">206</span>
               
            </h2>
            <p>
                % <?= __('Increase in revenue')?>
            </p>
        </div>
        <div class="cell large-3">
            
                
            <h2>
                <div class="icon-box"><i class="fi-dollar"></i></div>
                <span class="counter">45,000</span>
                
            </h2>
            <p>
                <?= __('Payments since we began')?>
            </p>
            
                
            
        </div>
        <div class="cell large-3">
            
                
            <h2>
                <div class="icon-box"><i class="fi-like"></i></div>
                <span class="counter">78</span>
                
            </h2>
            <p>
                <?= __('Countries')?>
            </p>
            
                
            
        </div>
    </div>
</div>



<div id="plans">
    <div class="grid-x">
        <div class="cell large-12">
            <h3 class="header-center-margin"><?= __('Plans')?></h3>
            <span class="separator"></span>
        </div>
    </div>

 

    <div class="grid-x">
        <?php  echo $this->element('plans', $results)?>
    </div>
</div>



<div id="testimonial">
    <div class="grid-x">
        <div class="cell large-12">
            <h3 class="header-center-margin"><?= __('testimonial')?></h3>
            <span class="separator"></span>
        </div>
    </div>
    <div class="grid-x home-content text-center">
        <div class="cell large-12">
            <?= $this->Html->image('man-smile-testimonial.png')?>
            <blockquote>

                I have a 9-5 working job, when I found this site my income increased and now I can afford things I couldn't before
                <cite>Andrew</cite>
            </blockquote>
            
        </div>
    </div>
</div>



<div id="payment-gateways">
    <div class="grid-x">
        <div class="cell large-12">
            <h3 class="header-center-margin"><?= __('Payment gateways')?></h3>
            <span class="separator"></span>
        </div>
    </div>
    <div class="grid-x text-center">
        <div class="cell large-4">
            <?= $this->Html->image('banner4.png') ?>
        </div>
        <div class="cell large-4">
            <?= $this->Html->image('banner5.png') ?>
        </div>
        <div class="cell large-4">
            <?= $this->Html->image('btc.png') ?>
        </div>
    </div>
</div>

<div id="footer">
    <div class="grid-x text-center">
        <div class="cell large-4">
           
            <p>
                <ul class="social">
                <li> <a href="#"><i class="fi-social-facebook"></i></a>  </li>
                <li> <a href="#"> <i class="fi-social-twitter"></i> </a> </li>
                <li> <a href="#"> <i class="fi-social-instagram"></i> </a> </li>
            </ul>
            </p>
        </div>
        <div class="cell large-4">
            
          <span class="social-email">
            <p>
                
                <?php echo Configure::read('adminEmail'); ?>
            </p>
        </span>
            
        </div>
        <div class="cell large-4">
            
                <span class="social-email">
                    <p>
                         
                57 Stanley Road <br>
                YORK <br>
                YO97 1XD <br>
                    </p>
               
            </span>
            
        </div>
    </div>
</div>

<!-- <div id="remind-cta">
    <div class="grid-x home-content text-center">
        <div class="cell large-12">
            <button>Join</button>
            <button>Login</button>
        </div>
        
    </div>
</div> -->

<!--  -->
<?php 
echo $this->fetch('scriptBottom');
?>
<script>
    
jQuery(document).ready(function(){
    $('.counter').counterUp({
    delay: 10,
    time: 1000
});
})

</script>
</body>
</html>
