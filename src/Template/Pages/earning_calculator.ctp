<?php 

$this->layout = 'no_layout';
?>
<?php
use Cake\Core\Configure;
use Cake\Utility\Text;
$debug = Configure::read('debug');
?>
<?php if($debug): ?>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<?php else:  ?>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>
<?php endif;?>
<!-- <script src="https://api.dmcdn.net/all.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->

<section class="intro">
<?php 
echo $this->element('top_bar_with_logo');
?>
	<div id = "app" class="grid-x playlist-grid">
	
	<div  class="cell large-6">
		<h3><?= __('Earning calculator')?></h3>
				<p><?= __('My account')?></p>
				<select v-model="mySelected">
				  <option disabled value="">Please select one</option>
					  <option v-for="package in items" v-bind:value="package.cost_per_video">
					  	{{package.name}}
					  
					  </option>
				  
				</select>
				
				
				<label><?=__('Videos Watched')?>
					<div class="input-group">
						<span class="input-group-label" v-on:click="removeWatched">-</span >
							<input class="input-group-field" type="number" v-model="watched">
						<span class="input-group-label" v-on:click="addWatched">+</span >
					</div>
				</label>

				<p><?= __('My referral subscription')?></p>
				<select v-model="referralSelected">
				  <option disabled value="">Please select one</option>
					  <option v-for="package in items" v-bind:value="package.commission">
					  	{{package.name}}
					  
					  </option>
				  
				</select>
				
				<label><?=__('Referrals Videos Watched')?>
					<div class="input-group">
						<span class="input-group-label" v-on:click="removeReferralsWatched">-</span >
							<input class="input-group-field" type="number" v-model="referralsWatched">
						<span class="input-group-label" v-on:click="addReferralWatched">+</span >
					</div>
				</label>

				<label><?=__('Referral Count')?>
					<div class="input-group">
						<span class="input-group-label" v-on:click="removeReferral">-</span >
							<input class="input-group-field" type="number" v-model="referrals">
						<span class="input-group-label" v-on:click="addReferral">+</span >
					</div>
				</label>
			
		
	</div>
	<div class="cell large-6" style="text-align: center;">
		<h3><?= __('Weekly earnings')?></h3>
		
			<p><?=__('Me')?> : <span>{{myMultiplier}}</span></p>
			<p><?=__('Referrals')?> : <span> {{ refs }} </span></p>
			
		
		<strong><p><?=__('Total')?> : {{ total }} </p></strong>
	</div>
</div>
</section>

<script type="text/javascript">
	var foo = <?php echo json_encode(($packagesQuery));?>;
	
	var app = new Vue({
		el: '#app',
		data :{

			watched : 0,
			referrals : 0,
			referralsWatched : 0,
			mySelected : '',
			referralSelected :'',
			items : foo,
			
		},
		methods : {
			addWatched : function(){
				
				this.watched =  this.watched+1;
				
			},
			removeWatched : function(){
				this.watched = this.watched-1;
			},
			addReferral : function(){
				
				this.referrals =  this.referrals+1;
				
			},
			removeReferral : function(){
				this.referrals = this.referrals-1;
			},
			addReferralWatched : function(){
				
				this.referralsWatched =  this.referralsWatched+1;
				
			},
			removeReferralsWatched : function(){
				this.referralsWatched = this.referralsWatched-1;
			},
			weekly : function(){
				return 7;
			},
			
			
		},
		computed : {
			myMultiplier : function(){
				var some = (this.watched * this.mySelected)*this.weekly();
				return  some.toFixed(2);
			},
			referralCounter : function(){
				//get multiplied
				var boo = this.watched;
				var who = this.referrals;
				return boo*who;
			},
			total : function(){
				var total = parseFloat(this.refs) + parseFloat(this.myMultiplier) ;
				return total.toFixed(2);
				
				
			},
			refs:function(){
				var some = this.referrals* this.referralsWatched * (this.referralSelected/100) *this.mySelected *this.weekly();
				return  some.toFixed(2);
			}
		}

	})
</script>