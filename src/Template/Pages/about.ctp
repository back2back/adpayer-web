<?php 

$this->layout = 'no_layout';
?>

<section class="intro">
	<?php 

    echo $this->element('top_bar_with_logo');
?>
	<div class="grid-x playlist-grid">
		<div class="cell large-12">
			<h2><?= __('About Us')?></h2>
		</div>
	</div>
</section>