<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReferralEarning $referralEarning
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Referral Earning'), ['action' => 'edit', $referralEarning->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Referral Earning'), ['action' => 'delete', $referralEarning->id], ['confirm' => __('Are you sure you want to delete # {0}?', $referralEarning->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Referral Earnings'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Referral Earning'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="referralEarnings view large-9 medium-8 columns content">
    <h3><?= h($referralEarning->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Parent') ?></th>
            <td><?= h($referralEarning->parent) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Child') ?></th>
            <td><?= h($referralEarning->child) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($referralEarning->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->format($referralEarning->amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($referralEarning->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($referralEarning->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Referral Status') ?></th>
            <td><?= $referralEarning->referral_status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
