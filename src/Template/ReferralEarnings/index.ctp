<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ReferralEarning[]|\Cake\Collection\CollectionInterface $referralEarnings
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Referral Earning'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="referralEarnings index large-9 medium-8 columns content">
    <h3><?= __('Referral Earnings') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('parent') ?></th>
                <th scope="col"><?= $this->Paginator->sort('child') ?></th>
                <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('referral_status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($referralEarnings as $referralEarning): ?>
            <tr>
                <td><?= $this->Number->format($referralEarning->id) ?></td>
                <td><?= h($referralEarning->parent) ?></td>
                <td><?= h($referralEarning->child) ?></td>
                <td><?= $this->Number->format($referralEarning->amount) ?></td>
                <td><?= h($referralEarning->referral_status) ?></td>
                <td><?= h($referralEarning->created) ?></td>
                <td><?= h($referralEarning->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $referralEarning->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $referralEarning->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $referralEarning->id], ['confirm' => __('Are you sure you want to delete # {0}?', $referralEarning->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
