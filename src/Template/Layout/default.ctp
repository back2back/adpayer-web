<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$siteDescription = 'Earn online by watching videos';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="adpayer, video, ptc,watch,ads,network,earn,cash,money,online">
    <title>
        <?php
        $siteDescription = "adpayer: earn by watching videos at home";
         echo $siteDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    
    <?= $this->Html->css('foundation.min.css') ?>
    <?= $this->Html->css('app.css') ?>
     <?= $this->Html->css('app2.css') ?>
    <?= $this->Html->css('foundation-icons.css') ?>
    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css') ?>
    
    

    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
     <?php 
        echo $this->Html->script('vendor/jquery', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/what-input', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/foundation', array('block' => 'scriptBottom'));
        echo $this->Html->script('app.js', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/jquery.waypoints.min', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/jquery.counterup.min', array('block' => 'scriptBottom'));
    ?> 

<?php
use Cake\Core\Configure;
use Cake\Utility\Text;
$debug = Configure::read('debug');
?>
<?php if($debug): ?>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<?php else:  ?>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>
<?php endif;?>
<!-- <script src="https://api.dmcdn.net/all.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->

<script type="text/javascript"> window.$crisp=[];window.CRISP_WEBSITE_ID="f4c6dc9d-de0d-4b8f-a4b7-e6c4616894ba";(function(){ d=document;s=d.createElement("script"); s.src="https://client.crisp.chat/l.js"; s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})(); </script>

</head>
<body>
  <div class="logged-background">
    
  
    <div class="title-bar" data-responsive-toggle="site-menu" data-hide-for="medium">
      <button class="menu-icon" type="button" data-toggle="site-menu"></button>
      <div class="title-bar-title">Menu</div>
    </div>

    <!-- <div class="top-bar" id="site-menu">
      <div class="top-bar-left">
        <ul class="dropdown menu" data-dropdown-menu>
          <li class="menu-text">adPayer</li>
         
        </ul>
      </div>
      <div class="top-bar-right">
       
        <?php 
       
        
        echo $this->element('user_details_logged');
       
        ?>
      </div>
    </div> -->
    <?php 

  echo $this->element('top_bar_with_logo');
?>


    <?= $this->Flash->render() ?>

    <div class="container clearfix ">
     
     
        <?php  
          echo $this->fetch('content'); 
      ?>  
      
      
      
    </div>
  </div>
    <footer>

<?php echo $this->fetch('scriptBottom');?>
    </footer>
</body>
</html>
