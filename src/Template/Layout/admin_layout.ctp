<?php
$this->extend('/Layout/default');
?>
<!DOCTYPE html>
<html>
<div class="grid-x">
	<div class="large-3 cell">
		<?php 
		//if ($loggedIn) {
			# code...
			echo $this->element('admin_side_nav');
		//}
		?>
	</div>
	<div class="large-9 cell">
		<?= $this->fetch('content') ?>
	</div>
</div>
</html>