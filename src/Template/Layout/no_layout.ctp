<?php
?>
<!DOCTYPE html>
<html class="no-js">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="adpayer, video, ptc,watch,ads,network,earn,cash,money,online">
    <title>
        <?php
        $siteDescription = "adpayer: earn by watching videos at home";
         echo $siteDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    
    <?= $this->Html->css('foundation.min.css') ?>
    <?= $this->Html->css('app.css') ?>
     <?= $this->Html->css('app2.css') ?>
    <?= $this->Html->css('foundation-icons.css') ?>
    <?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css') ?>
    
    

    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
     <?php 
        echo $this->Html->script('vendor/jquery', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/what-input', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/foundation', array('block' => 'scriptBottom'));
        echo $this->Html->script('app.js', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/jquery.waypoints.min', array('block' => 'scriptBottom'));
        echo $this->Html->script('vendor/jquery.counterup.min', array('block' => 'scriptBottom'));
    ?> 

<script type="text/javascript"> window.$crisp=[];window.CRISP_WEBSITE_ID="f4c6dc9d-de0d-4b8f-a4b7-e6c4616894ba";(function(){ d=document;s=d.createElement("script"); s.src="https://client.crisp.chat/l.js"; s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})(); </script>
</head>

<body>
    
<div class="title-bar" data-responsive-toggle="site-menu" data-hide-for="medium">
  <button class="menu-icon" type="button" data-toggle="site-menu"></button>
  <div class="title-bar-title">Menu</div>
</div>


	<div class="grid-x">
		<div class="cell large-12">
      
			<?= $this->fetch('content')?>

		</div>
	</div>

    
</div>

<?php 
echo $this->fetch('scriptBottom');
?>
</body>
</html>







