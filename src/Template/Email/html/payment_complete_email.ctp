<?php
?>

<h3><strong><?= __('Payment received on Adpayer')?></strong></h3>
<p>
	<?php  echo __('Hi '. $userData->f_name)?>
</p>
<p>
	<?php echo __('Thank you for your payment to Adpayer. Your account is now upgraded to view more videos and earn more commissions from referrals. If you have any query please do not hesitate to contact us. Here are more details about your purchase.');?>
</p>
<ul style="list-style-type: none;">
	<li><?= __('Package name')?><?= $packageData->name?></li>
	<li><?= __('Cost in USD')?> <?= $packageData->package_cost?> </li>
	<li><?= __('Commission percentage')?> <?= $packageData->commission?></li>
	<li><?= __('Earning per video in USD')?> <?= $packageData->cost_per_video?></li>
	<li><?= __('Max videos per day')?> <?= $packageData->video_load?></li>
	<li></li>
</ul>



<p><?= __('Best Regards')?></p>