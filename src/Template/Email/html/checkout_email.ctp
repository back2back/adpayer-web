<?php
?>
<p>
	<?php  echo __('Hi '. $userData->f_name)?>
</p>
<p>
	<?php echo __('Here are instructions to pay directly to our BTC address.');?>
</p>
<p> <?= __('Amount to pay. Please pay full amount')?></p>
<p><strong><?= $checkoutData->amount?></strong></p>

<p> <?= __('BTC address')?></p>
<p> <strong><?= $checkoutData->address?></strong></p>

<p> <?= __('Please copy paste the address and the amount to your BTC wallet to avoid errors')?></p>

<p><?= __('You can also click on the link below if your wallet supports QR codes.')?></p>
<p><?= $checkoutData->checkout_url ?></p>

<p><?= __('Thank you')?></p>