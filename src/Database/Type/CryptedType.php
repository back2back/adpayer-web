<?php
namespace App\Database\Type;

use Cake\Database\Driver;
use Cake\Database\Type;
use Cake\Utility\Security;
use Cake\Core\Configure;
use Cake\Log\Log;
use PDO;

class CryptedType extends Type
{
    
	public function toPHP($value, Driver $driver)
    {

        if ($value === null) {
        	
            return null;
        }


        $data = Security::decrypt($value, Security::salt());
        Log::write('error', 'type of object '. gettype($data));
        Log::write('error', 'in object'. is_null($data));
        return $data;
        
    }

    public function marshal($value)
    {
        if (is_array($value) || $value === null) {
            return $value;
        }
        $address =  Security::decrypt($value, Security::salt());
        Log::write('error', 'coin key marshal' . $address);
        return $address;
    }

    public function toDatabase($value, Driver $driver)
    {
        return Security::encrypt($value, Security::salt());
    }

    
    

    public function toStatement($value, Driver $driver)
    {
    	Log::write('error', 'to Statement '. $value);
        if ($value === null) {
            return PDO::PARAM_NULL;
        }
        return PDO::PARAM_STR;
    }
}