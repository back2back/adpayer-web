<?php
namespace App\Event;

use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use App\Utility\CommissionCalculator;

class ReferralEarnings implements EventListenerInterface
{
    public function implementedEvents()
    {
        return [
            'Model.Playlist.afterSave' => 'updateReferralEarnings',
        ];
    }

    public function updateReferralEarnings($event)
    {
        //Log::write('error', 'Received event ' . $event->getData('playlist'));
        $playlist = $event->getData('playlist');
        $amount = $playlist->earn;
        $userId = $playlist->user_id;
        
        //$refEarnings = TableRegistry::getTableLocator()->get('ReferralEarnings');
        //$query = $refEarnings->find();
        $commss = new CommissionCalculator();
        $commss->getParentDetails($userId, $amount);

    }
}

