<?php
namespace App\Event;

use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;
use Cake\Log\LogTrait;
use Cake\Mailer\Email;
use Cake\Core\Configure;
use Cake\Routing\Router;


class EmailListener implements EventListenerInterface
{
    use LogTrait;
	public function implementedEvents()
    {
        return [
            'Model.Email.PurchaseConfirmed' => 'sendEmailPurchaseConfirmed',
            'Model.Email.ResetToken' => 'sendEmailResetToken',
            'Model.Email.PurchaseBegan' => 'sendEmailPurchaseBegan'

        ];
    }

    public function emailHandler($userEntity, $emailFrom){
        $this->log('email handler called');
        
       
        $email = new Email('mailgun');
        $email
            ->setEmailFormat('html')
            ->setFrom([Configure::read('adminEmail') => $emailFrom])
            ->setTo($userEntity->email);


        return $email;
    }

    public function sendEmailPurchaseBegan($event){

        // Already have the payment provider details
        // get the user entity with email
        $payment = $event->getData('payment');
        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $userEntity = $usersTable->get($payment->user_id);

        $from = __('Payment due');
        $email = $this->emailHandler($userEntity, $from);
        $email
            ->setViewVars(['checkoutData' => $payment, 'userData' => $userEntity])
            ->template('checkout_email')
            ->setSubject(__('How to pay'))
            ->send();
    }
    public function sendEmailPurchaseConfirmed($event)
    {
    	$purchaseEntity = $event->getData('purchase');

        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $userEntity = $usersTable->get($purchaseEntity->user_id);

        $packagesTable = TableRegistry::getTableLocator()->get('Packages');
        $packageEntity = $packagesTable->get($purchaseEntity->package_id);

        $from = __('Payment completed');
        $email = $this->emailHandler($userEntity, $from);
        $email
            ->setViewVars([
                'purchaseData' => $purchaseEntity, 
                'userData' => $userEntity,
                'packageData' => $packageEntity
            ])
            ->template('payment_complete_email')
            ->setSubject(__('Payment completed'))
            ->send();
        
        $this->log('purchase confirmed');        
    	
    }
    public function sendEmailResetToken($event){
        $entity = $event->getData('reset');
        
        $domain = Router::url('/', true);
        $resetUrl = $domain.'reset-tokens/validate/'.$entity->token;

        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $queryUser = $usersTable->findByEmail($entity->email);
        
        $userData = $queryUser->first();
       
        $from = __('Password Reset');
        $email = $this->emailHandler($userData, $from);
        $email
            ->setViewVars(['url' => $resetUrl , 'userData' => $userData])
            ->template('reset_email')
            ->setSubject(__('Password recovery'))
            ->send();

    }

    
}