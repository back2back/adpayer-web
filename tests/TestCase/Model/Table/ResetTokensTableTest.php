<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ResetTokensTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ResetTokensTable Test Case
 */
class ResetTokensTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ResetTokensTable
     */
    public $ResetTokens;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ResetTokens'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ResetTokens') ? [] : ['className' => ResetTokensTable::class];
        $this->ResetTokens = TableRegistry::getTableLocator()->get('ResetTokens', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ResetTokens);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
