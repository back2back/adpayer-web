<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PayoutsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PayoutsTable Test Case
 */
class PayoutsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PayoutsTable
     */
    public $Payouts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Payouts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Payouts') ? [] : ['className' => PayoutsTable::class];
        $this->Payouts = TableRegistry::getTableLocator()->get('Payouts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Payouts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
