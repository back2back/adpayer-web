<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CoinpaymentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CoinpaymentsTable Test Case
 */
class CoinpaymentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CoinpaymentsTable
     */
    public $Coinpayments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Coinpayments',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Coinpayments') ? [] : ['className' => CoinpaymentsTable::class];
        $this->Coinpayments = TableRegistry::getTableLocator()->get('Coinpayments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Coinpayments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
