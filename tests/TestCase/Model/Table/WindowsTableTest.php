<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WindowsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WindowsTable Test Case
 */
class WindowsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WindowsTable
     */
    public $Windows;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Windows',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Windows') ? [] : ['className' => WindowsTable::class];
        $this->Windows = TableRegistry::getTableLocator()->get('Windows', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Windows);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
