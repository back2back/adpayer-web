<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReferralEarningsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReferralEarningsTable Test Case
 */
class ReferralEarningsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ReferralEarningsTable
     */
    public $ReferralEarnings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ReferralEarnings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ReferralEarnings') ? [] : ['className' => ReferralEarningsTable::class];
        $this->ReferralEarnings = TableRegistry::getTableLocator()->get('ReferralEarnings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReferralEarnings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
